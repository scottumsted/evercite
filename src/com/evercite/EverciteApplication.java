package com.evercite;

import android.content.Context;
import android.view.Gravity;
import android.widget.Toast;
import com.evercite.model.Ec;
import com.evercite.model.EverciteModel;
import com.evercite.model.Topic;
import com.evercite.model.Work;

import android.app.Application;

public class EverciteApplication extends Application {
	private EverciteModel model;
	private Topic focusTopic=null;
	private Work focusWork=null;
	private Ec ec=null;
	private int filterTopicId = -1;
	private int selectedTopicId = -1;
	private int selectedWorkId = -1;
	private String intentIsbn = "";
	
	@Override
	public void onCreate() {
		super.onCreate();
		model = new EverciteModel(this);
		ec = model.getEc();
	}

    public void showQuickToast(String message){
        Context context = getApplicationContext();
        CharSequence text = (CharSequence)message;
        int duration = Toast.LENGTH_SHORT;
        Toast toast = Toast.makeText(context, text, duration);
        toast.setGravity(Gravity.TOP, 0, 0);
        toast.show();
    }

    public EverciteModel getModel() {
		return model;
	}
	public void setModel(EverciteModel model) {
		this.model = model;
	}
	public Topic getFocusTopic() {
		return focusTopic;
	}
	public void setFocusTopic(Topic focusTopic) {
		this.focusTopic = focusTopic;
	}
	public Work getFocusWork() {
		return focusWork;
	}
	public void setFocusWork(Work focusWork) {
		this.focusWork = focusWork;
	}
	public Ec getEc() {
		return ec;
	}
	public void setEc(Ec ec) {
		this.ec = ec;
	}
	public int getFilterTopicId() {
		return filterTopicId;
	}
	public void setFilterTopicId(int filterTopicId) {
		this.filterTopicId = filterTopicId;
	}
	public int getSelectedTopicId() {
		return selectedTopicId;
	}
	public void setSelectedTopicId(int selectedTopicId) {
		this.selectedTopicId = selectedTopicId;
	}
	public int getSelectedWorkId() {
		return selectedWorkId;
	}
	public void setSelectedWorkId(int selectedWorkId) {
		this.selectedWorkId = selectedWorkId;
	}
	public String getIntentIsbn() {
		return intentIsbn;
	}
	public void setIntentIsbn(String intentIsbn) {
		this.intentIsbn = intentIsbn;
	}
	
}
