package com.evercite;

import java.util.List; 

import com.evercite.model.EverciteModel;
import com.evercite.model.Topic;
import android.app.Dialog;
import android.app.ListActivity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Rect;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class TopicsActivity extends ListActivity {

	private static final int TOPIC_VIEW_DIALOG_ID = 0;
	private static final int TOPIC_CONFIRM_DIALOG_ID = 1;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        List<Topic> topics = getModel().getTopics();
        ListView listView = getListView();
        listView.setCacheColorHint(R.color.light);
		LayoutInflater inflater = getLayoutInflater();
		ViewGroup header = (ViewGroup)inflater.inflate(R.layout.topics_main, listView, false);
		listView.addHeaderView(header,null,false);        
        
		final TopicArrayAdapter topicArrayAdapter = new TopicArrayAdapter(this,R.layout.topic_item_row,topics);
        this.setListAdapter(topicArrayAdapter);

        listView.setTextFilterEnabled(true);
        listView.setBackgroundResource(R.color.light);
        listView.setCacheColorHint(Color.parseColor("#00000000"));

        final ImageButton addButton = (ImageButton) findViewById(R.id.add);
        addButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                final EditText topicEntry = (EditText) findViewById(R.id.topicEntry);
                String name = topicEntry.getEditableText().toString();
                if(name!=null && name.length()>0){
                    Topic topic = new Topic();
                    topic.setName(name);
                    getModel().addTopic(topic);
                    topicEntry.setText("");
                    topicEntry.clearFocus();
                    refreshTopics(topicArrayAdapter);
                    InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(topicEntry.getWindowToken(), 0);
                    //showQuickToast(topic.getName()+" added");
                }else{
                	showQuickToast("Please enter a topic name");
                }
            }
        });
 
    }

    private void showQuickToast(String message){
    	Context context = getApplicationContext();
    	CharSequence text = (CharSequence)message;
    	int duration = Toast.LENGTH_SHORT;
    	Toast toast = Toast.makeText(context, text, duration);
    	toast.setGravity(Gravity.TOP, 0, 0);
    	toast.show();	
    }
    
    protected EverciteModel getModel(){
        return ((EverciteApplication)getApplication()).getModel();
    }

    protected void refreshTopics(TopicArrayAdapter topicArrayAdapter){
        List<Topic> topics = getModel().getTopics();
        topicArrayAdapter.clear();
        for(Topic t: topics){
        	topicArrayAdapter.add(t);
        }
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
        case TOPIC_VIEW_DIALOG_ID:
        	return this.buildViewDialog();
     
        case TOPIC_CONFIRM_DIALOG_ID:
        	return this.buildConfirmDialog();
        }
        return null;
    }
    
    @Override
    protected void onPrepareDialog(int id, Dialog dialog) {
        super.onPrepareDialog(id, dialog);
        Topic topic = getModel().getTopic(((EverciteApplication)this.getApplication()).getSelectedTopicId());
        switch (id) {
        case TOPIC_VIEW_DIALOG_ID:
        	EditText topicEdit = (EditText)dialog.findViewById(R.id.topicName);
        	topicEdit.setText(topic.getName()); 
        	break;
        case TOPIC_CONFIRM_DIALOG_ID:
        	TextView topicView = (TextView)dialog.findViewById(R.id.removeDescription);
        	topicView.setText(topic.getName());    	
            break;
        }
    }

    
    public Dialog buildConfirmDialog(){
    	final Dialog dialog = new Dialog(this);
    	dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
    	Rect dimensions = new Rect();
    	Window window = this.getWindow();
    	window.getDecorView().getWindowVisibleDisplayFrame(dimensions);
    	dialog.setContentView(R.layout.confirm_dialog);
    	dialog.getWindow().setLayout((int)(dimensions.width()*0.9f), (int)(dimensions.height()*0.4f));
    	TextView workView = (TextView)dialog.findViewById(R.id.removeLabel);
    	workView.setText("Remove Topic?");
    	TextView topicView = (TextView)dialog.findViewById(R.id.removeDescription);
    	topicView.setText("Topic");
    	Button yesButton = (Button)dialog.findViewById(R.id.removeYes);
    	yesButton.setOnClickListener(
    			new View.OnClickListener(){
	                    public void onClick(View view) {
	                    	TopicsActivity topicsActivity = (TopicsActivity)dialog.getOwnerActivity();
	                    	EverciteModel model = ((EverciteApplication)(getApplication())).getModel();
	                    	TopicArrayAdapter topicArrayAdapter = (TopicArrayAdapter)topicsActivity.getListAdapter();
	                    	
	                    	Topic topic = model.getTopic(((EverciteApplication)topicsActivity.getApplication()).getSelectedTopicId());
	                    	model.removeTopic(topic);
	                    	topicsActivity.refreshTopics(topicArrayAdapter);
	                    	dialog.dismiss();
	                    }
                    }
    			);
    	Button noButton = (Button)dialog.findViewById(R.id.removeNo);
    	noButton.setOnClickListener(
    			new View.OnClickListener(){
	                    public void onClick(View view) {
	                    	dialog.dismiss();
	                    }
                    }
    			);
    	return dialog;
    }

    public Dialog buildViewDialog(){
    	final Dialog dialog = new Dialog(this);
    	dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
    	Rect dimensions = new Rect();
    	Window window = this.getWindow();
    	window.getDecorView().getWindowVisibleDisplayFrame(dimensions);
    	dialog.setContentView(R.layout.topic_view_dialog);
    	dialog.getWindow().setLayout((int)(dimensions.width()*0.9f), (int)(dimensions.height()*0.5f));
    	EditText topicEdit = (EditText)dialog.findViewById(R.id.topicName);
    	topicEdit.setText("Topic");
    	Button saveButton = (Button)dialog.findViewById(R.id.topicSave);
    	saveButton.setOnClickListener(
    			new View.OnClickListener(){
	                    public void onClick(View view) {
	                    	TopicsActivity topicsActivity = (TopicsActivity)dialog.getOwnerActivity();
	                    	EverciteModel model = ((EverciteApplication)(getApplication())).getModel();
	                    	TopicArrayAdapter topicArrayAdapter = (TopicArrayAdapter)topicsActivity.getListAdapter();
	                    	EditText topicEdit = (EditText)dialog.findViewById(R.id.topicName);
	                    	Topic topic = model.getTopic(((EverciteApplication)topicsActivity.getApplication()).getSelectedTopicId());
	                    	topic.setName(topicEdit.getText().toString());
	                    	model.updateTopic(topic);
	                    	topicsActivity.refreshTopics(topicArrayAdapter);
	                    	dialog.dismiss();
	                    }
                    }
    			);
    	Button cancelButton = (Button)dialog.findViewById(R.id.topicCancel);
    	cancelButton.setOnClickListener(
    			new View.OnClickListener(){
	                    public void onClick(View view) {
	                    	dialog.dismiss();
	                    }
                    }
    			);
    	Button viewButton = (Button)dialog.findViewById(R.id.topicFilter);
    	viewButton.setOnClickListener(
    			new View.OnClickListener(){
	                    public void onClick(View view) {
	                    	EverciteApplication everciteApplication = (EverciteApplication)(dialog.getOwnerActivity().getApplication());
	                    	everciteApplication.setFilterTopicId(everciteApplication.getSelectedTopicId());
	                    	((TopicsActivity)dialog.getOwnerActivity()).switchTab(EverciteActivity.WORKS_TAB);
	                    	dialog.dismiss();
	                    }
                    }
    			);
    	return dialog;
    }

    public void showViewDialog(int topicId){
    	((EverciteApplication)this.getApplication()).setSelectedTopicId(topicId);
    	showDialog(TOPIC_VIEW_DIALOG_ID);
    }

    public void showConfirmDialog(int topicId){
    	((EverciteApplication)this.getApplication()).setSelectedTopicId(topicId);
    	showDialog(TOPIC_CONFIRM_DIALOG_ID);
    }
    
    public void switchTab(int tabId){
        ((EverciteActivity)this.getParent()).switchTab(tabId);
}
    	
}
