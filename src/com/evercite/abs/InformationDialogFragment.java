package com.evercite.abs;

import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.actionbarsherlock.app.SherlockDialogFragment;
import com.evercite.EverciteApplication;
import com.evercite.R;
import com.evercite.model.EverciteModel;
import com.evercite.model.Work;
import com.river.RiverLookup;

/**
 * Created with IntelliJ IDEA.
 * User: scottumsted
 * Date: 8/17/2012
 * Time: 2:05 PM
 * To change this template use File | Settings | File Templates.
 */
public class InformationDialogFragment extends SherlockDialogFragment {

    static InformationDialogFragment newInstance(String message) {
        InformationDialogFragment f = new InformationDialogFragment();

        Bundle args = new Bundle();
        args.putString("message", message);
        f.setArguments(args);

        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        // load model here if needed
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        super.onCreate(savedInstanceState);

        View view = inflater.inflate(R.layout.information_dialog, container, false);


        // set event listeners
        Button acknowledgeButton = (Button)view.findViewById(R.id.acknowledge);
        acknowledgeButton.setOnClickListener(
                new View.OnClickListener(){
                    public void onClick(View view) {
                        // do something here, maybe with the model, etc.
                        getDialog().dismiss();
                    }
                }
        );

        // populate view
        ((TextView)view.findViewById(R.id.informationMessage)).setText(getArguments().getString("message"));

        return view;
    }

}
