package com.evercite.abs;

import android.app.ListFragment;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import com.actionbarsherlock.app.SherlockListFragment;
import com.evercite.EverciteApplication;
import com.evercite.R;
import com.evercite.WorkArrayAdapter;
import com.evercite.model.EverciteModel;
import com.evercite.model.Work;
import com.river.RiverLookup;
import com.river.RiverResults;
import com.river.RiverWork;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: scottumsted
 * Date: 7/7/12
 * Time: 12:55 PM
 * To change this template use File | Settings | File Templates.
 */
public class WorksFragment extends SherlockListFragment{

    private static final int WORK_VIEW_DIALOG_ID = 0;
    private static final int WORK_CONFIRM_DIALOG_ID = 1;
    private static final int WORK_RIVER_DIALOG_ID = 2;
    private static final int MAX_LOOKUP_TRIES = 3;
    private int selectedWorkId = -1;

    static WorksFragment newInstance() {
        WorksFragment f = new WorksFragment();

        // Supply num input as an argument.
        //Bundle args = new Bundle();
        //args.putInt("num", num);
        //f.setArguments(args);

        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        List<Work> works= getModel().getWorks();
        final WorkABSArrayAdapter workArrayAdapter = new WorkABSArrayAdapter(getActivity(),R.layout.work_item_row,works);
        this.setListAdapter(workArrayAdapter);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState){
        super.onCreateView(inflater, container, savedInstanceState);

        ListView listView = new ListView(this.getActivity());
        listView.setId(android.R.id.list);
        listView.setCacheColorHint(R.color.light);
        //listView.setDivider( null );
        //listView.setDividerHeight(0);

        View  header = (ViewGroup)inflater.inflate(R.layout.works_main, listView, false);
        // Add Button Click
        final ImageButton addButton = (ImageButton) header.findViewById(R.id.add);
        addButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                final EditText isbnEntry = (EditText) getView().findViewById(R.id.isbnEntry);
                String isbn = isbnEntry.getEditableText().toString();
                if(isbn!=null && isbn.length()>0){
                    new GetBook().execute(isbn);
                }else{
                    showQuickToast(getString(R.string.addBookISBNError));
                }
            }
        });

        // Scan Button Clicked
        final ImageButton scanButton = (ImageButton) header.findViewById(R.id.scan);
        scanButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                final EditText isbnEntry = (EditText) getView().findViewById(R.id.isbnEntry);
                isbnEntry.setText("");
                Intent intent = new Intent("com.google.zxing.client.android.SCAN");
                intent.putExtra("SCAN_MODE", "PRODUCT_MODE");
                startActivityForResult(intent, 0);
            }
        });
        //listView.addHeaderView(header, null, false);

        listView.setTextFilterEnabled(true);
        listView.setBackgroundResource(R.color.light);
        listView.setCacheColorHint(Color.parseColor("#00000000"));

        // Click on position within list view, override by adapter click
        listView.setOnItemClickListener(
                new AdapterView.OnItemClickListener(){
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        showQuickToast("item "+position+"clicked");
                    }
                }
        );

        return listView;
    }


    /**
     * AsynTask to look up a book by isbn and refresh the works list.
     * TODO: add progress
     */
    private class GetBook extends AsyncTask<String, Void, RiverResults> {

        protected RiverResults doInBackground(String... isbn) {
            if(isbn!=null && isbn.length>0){
                RiverLookup rl = new RiverLookup();
                RiverResults rr  = null;
                for(int i=0;i<MAX_LOOKUP_TRIES;i++){
                    rr = rl.lookupISBN(isbn[0]);
                    if(rr.getStatus().equals(RiverResults.ITEM_FOUND)||rr.getStatus().equals(RiverResults.ITEM_NOT_FOUND)){
                        break;
                    }
                }
                return rr;
            }
            return null;
        }

        protected void onPostExecute(RiverResults rr) {
            if(rr.getStatus().equals(RiverResults.ITEM_FOUND)||rr.getStatus().equals(RiverResults.MULTIPLE_ITEMS_FOUND)){
                Work work = new Work();
                for(RiverWork rw :rr.getWorks()){
                    work.setIsbn(rw.getIsbn());
                    work.setAuthor(rw.getAuthor());
                    work.setCity(rw.getCity());
                    work.setPublisher(rw.getPublisher());
                    work.setTitle(rw.getTitle());
                    work.setUrl(rw.getUrl());
                    work.setYear(rw.getYear());
                    work.setMediumImage(rw.getMediumImage());
                    work.setSmallImage(rw.getSmallImage());
                    work.setLargeImage(rw.getLargeImage());
                    getModel().addWork(work);
                }
                ((EditText) getActivity().findViewById(R.id.isbnEntry)).setText("");
                refreshWorks();
                InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(((EditText) getActivity().findViewById(R.id.isbnEntry)).getWindowToken(), 0);
            }else if(rr.getStatus().equals(RiverResults.ITEM_NOT_FOUND)){
                ((EditText) getActivity().findViewById(R.id.isbnEntry)).setText("");
                showQuickToast(getString(R.string.addBookISBNError));
            }else if(rr.getStatus().equals(RiverResults.PARSE_EXCEPTION)){
                showQuickToast(getString(R.string.addBookParsingError));
            }else if(rr.getStatus().equals(RiverResults.COMMUNICATION_EXCEPTION)){
                showQuickToast(getString(R.string.addBookCommunicationError));
            }else if(rr.getStatus().equals(RiverResults.ERROR)){
                showQuickToast(getString(R.string.addBookGeneralError));
            }else if(rr.getStatus().equals(RiverResults.QUOTA_EXCEEDED)){
                showQuickToast(getString(R.string.addBookQuotaError));
            }

        }
    }

    /**
     * Show a toast quickly and at the top of the app.
     * Might replace with nice sherlock box.
     * @param message
     */
    private void showQuickToast(String message){
        ((EverciteApplication)getActivity().getApplication()).showQuickToast(message);
    }

    /**
     * Get the model object shared by the application.
     * @return
     */
    protected EverciteModel getModel(){
        return ((EverciteApplication)getActivity().getApplication()).getModel();
    }

    /**
     * Refresh the works adapter (and the list) using the wors stored in the db.
     */
    protected void refreshWorks(){
        List<Work> works = getModel().getWorks();
        ((WorkABSArrayAdapter)this.getListAdapter()).clear();
        for(Work w: works){
            ((WorkABSArrayAdapter)this.getListAdapter()).add(w);
        }
    }

}
