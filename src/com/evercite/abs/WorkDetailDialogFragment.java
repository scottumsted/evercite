package com.evercite.abs;

import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.actionbarsherlock.app.SherlockDialogFragment;
import com.evercite.EverciteApplication;
import com.evercite.R;
import com.evercite.model.EverciteModel;
import com.evercite.model.Work;
import com.river.RiverLookup;

/**
 * Created with IntelliJ IDEA.
 * User: scottumsted
 * Date: 7/8/12
 * Time: 9:44 PM
 * To change this template use File | Settings | File Templates.
 */
public class WorkDetailDialogFragment extends SherlockDialogFragment {

    static WorkDetailDialogFragment newInstance(int workId) {
        WorkDetailDialogFragment f = new WorkDetailDialogFragment();

        Bundle args = new Bundle();
        args.putInt("workId", workId);
        f.setArguments(args);

        return f;
    }

    private int workId = -1;
    private Work work = null;

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        workId = getArguments().getInt("workId");
        EverciteModel model = ((EverciteApplication)(getActivity().getApplication())).getModel();
        work = model.getWork(workId);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        super.onCreate(savedInstanceState);

        View view = inflater.inflate(R.layout.work_view_dialog, container, false);

        // set event listeners

        Button saveButton = (Button)view.findViewById(R.id.workSave);
        saveButton.setOnClickListener(
                new View.OnClickListener(){
                    public void onClick(View view) {
                        EverciteModel model = ((EverciteApplication)(getActivity().getApplication())).getModel();
                        work.setNote(((EditText)getDialog().findViewById(R.id.workNote)).getText().toString());
                        model.updateWork(work);
//                        ((EverciteABSActivity)getActivity()).refreshWorks();
                        getDialog().dismiss();
                    }
                }
        );

        Button cancelButton = (Button)view.findViewById(R.id.workCancel);
        cancelButton.setOnClickListener(
                new View.OnClickListener(){
                    public void onClick(View view) {
                        getDialog().dismiss();
                    }
                }
        );

        // populate view

        byte[] imageBytes=null;
        ImageButton imageButton = (ImageButton)view.findViewById(R.id.workImage);
        imageBytes = RiverLookup.decode(work.getMediumImage());
        imageButton.setImageBitmap(BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length));
        view.findViewById(R.id.workTitle).requestFocus();
        ((ScrollView)view.findViewById(R.id.workScroll)).fullScroll(ScrollView.FOCUS_UP);
        ((TextView)view.findViewById(R.id.workTitle)).setText(work.getTitle());
        ((TextView)view.findViewById(R.id.workAuthor)).setText(work.getAuthor());
        ((TextView)view.findViewById(R.id.workPublisher)).setText(work.getPublisher());
        ((TextView)view.findViewById(R.id.workYear)).setText(work.getYear());
        ((EditText)view.findViewById(R.id.workNote)).setText(work.getNote());
        view.findViewById(R.id.workNote).clearFocus();
        ((TextView)view.findViewById(R.id.workIsbn)).setText(work.getIsbn());

        return view;
    }

}
