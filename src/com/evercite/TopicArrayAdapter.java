package com.evercite;

import java.util.List;

import com.evercite.model.Topic;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.StateListDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.HeaderViewListAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;


public class TopicArrayAdapter extends ArrayAdapter<Topic>{

    Context context; 
    int layoutResourceId;    
    List<Topic> topics = null;
    
    public TopicArrayAdapter(Context context, int layoutResourceId, List<Topic> topics) {
        super(context, layoutResourceId, topics);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.topics = topics;
    }
    
    @Override
    public Topic getItem(int position){
    	return (topics!=null)?topics.get(position):null;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        TopicHolder holder = null;
        
        if(row == null)
        {
            LayoutInflater inflater = ((Activity)context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);
            row.setClickable(true);
            row.setFocusable(true);
            StateListDrawable stateListDrawable = new StateListDrawable();
            int[] states = new int[] { android.R.attr.state_pressed };
            stateListDrawable.addState(states, new ColorDrawable(0xFFD0D0D0));
            row.setBackgroundDrawable(stateListDrawable);
            row.setOnClickListener(
            		new View.OnClickListener(){
                        public void onClick(View view) {
                            RelativeLayout relativeLayout = (RelativeLayout)view;
                            ListView listView = (ListView)relativeLayout.getParent();
                            final int position = listView.getPositionForView(relativeLayout)-1;
                            if (position >= 0) {
                            	TopicArrayAdapter topicArrayAdapter = (TopicArrayAdapter)((HeaderViewListAdapter)listView.getAdapter()).getWrappedAdapter();
                                Topic topic = topicArrayAdapter.getItem(position);
                                ((TopicsActivity)context).showViewDialog(topic.getId());
                            }
                        }
           		});
            holder = new TopicHolder();
            holder.remove = (ImageButton)row.findViewById(R.id.remove);
            holder.txtName = (TextView)row.findViewById(R.id.txtName);
            holder.txtDateAdded = (TextView)row.findViewById(R.id.txtDateAdded);
            
            row.setTag(holder);
        }
        else
        {
            holder = (TopicHolder)row.getTag();
        }
        
        Topic topic = topics.get(position);
        holder.txtName.setText(topic.getName());
        holder.txtDateAdded.setText(topic.getPrettyDateAdded());
        holder.remove.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                RelativeLayout relativeLayout = (RelativeLayout)view.getParent();
                ListView listView = (ListView)relativeLayout.getParent();
                final int position = listView.getPositionForView(relativeLayout)-1;
                if (position >= 0) {
                	TopicArrayAdapter topicArrayAdapter = (TopicArrayAdapter)((HeaderViewListAdapter)listView.getAdapter()).getWrappedAdapter();
                    Topic topic = topicArrayAdapter.getItem(position);
                    ((TopicsActivity)context).showConfirmDialog(topic.getId());
                	/*
                    EverciteModel model = ((EverciteApplication)((TopicsActivity)listView.getContext()).getApplication()).getModel();
                	model.removeTopic(topic);
                	((TopicsActivity)listView.getContext()).refreshTopics(topicArrayAdapter);
                	CharSequence text = (CharSequence)topic.getName()+" removed";
                	int duration = Toast.LENGTH_SHORT;
                	Toast toast = Toast.makeText(view.getContext(), text, duration);
                	toast.show();
                	*/	
                }

            }
        });
        return row;
    }
    
    static class TopicHolder
    {
        ImageButton remove;
        TextView txtName;
        TextView txtDateAdded;
    }
}