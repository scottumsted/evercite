package com.evercite.abs;


import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;
import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragment;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.app.SherlockListFragment;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.evercite.EverciteApplication;
import com.evercite.R;
import com.evercite.model.EverciteModel;
import com.evercite.model.Work;
import com.river.RiverLookup;
import com.river.RiverResults;
import com.river.RiverWork;

/**
 * Created with IntelliJ IDEA.
 * User: scottumsted
 * Date: 7/5/12
 * Time: 11:59 PM
 * To change this template use File | Settings | File Templates.
 */
public class EverciteABSTabsActivity extends SherlockFragmentActivity {

    private static final int MAX_LOOKUP_TRIES = 3;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //Used to put dark icons on light action bar
        //boolean isLight = SampleList.THEME == R.style.Theme_Sherlock_Light;

        MenuItem menuItem = menu.add("Search");
        menuItem.setIcon(R.drawable.ic_search_inverse);
        menuItem.setActionView(R.layout.works_main);
        menuItem.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS | MenuItem.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW);
        final View searchView = menuItem.getActionView();

        // Add Button Click
        final ImageButton addButton = (ImageButton) searchView.findViewById(R.id.add);
        addButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                final EditText isbnEntry = (EditText) searchView.findViewById(R.id.isbnEntry);
                new GetBook(isbnEntry.getText().toString()).execute(true);
                //isbnEntry.setText("");
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(isbnEntry.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
            }
        });

        // Scan Button Clicked
        final ImageButton scanButton = (ImageButton) searchView.findViewById(R.id.scan);
        scanButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                final EditText isbnEntry = (EditText) searchView.findViewById(R.id.isbnEntry);
                isbnEntry.setText("");
                Intent intent = new Intent("com.google.zxing.client.android.SCAN");
                intent.putExtra("SCAN_MODE", "PRODUCT_MODE");
                startActivityForResult(intent, 0);
            }
        });

        return true;
    }

    private class GetBook extends AsyncTask<Boolean, Void, RiverResults> {
        String isbn;
        public GetBook(String isbn){
            this.isbn = isbn;
        }

        //protected RiverResults doInBackground(String... isbn) {
        protected RiverResults doInBackground(Boolean... doIt) {
            if(doIt!=null && doIt[0]){
                if(isbn!=null && isbn.length()>0){
                    RiverLookup rl = new RiverLookup();
                    RiverResults rr  = null;
                    for(int i=0;i<MAX_LOOKUP_TRIES;i++){
                        rr = rl.lookupISBN(isbn);
                        if(rr.getStatus().equals(RiverResults.ITEM_FOUND)||rr.getStatus().equals(RiverResults.ITEM_NOT_FOUND)){
                            break;
                        }
                    }
                    return rr;
                }
            }
            return null;
        }

        protected void onPostExecute(RiverResults rr) {
            if(rr != null){
                if(rr.getStatus().equals(RiverResults.ITEM_FOUND)||rr.getStatus().equals(RiverResults.MULTIPLE_ITEMS_FOUND)){
                    Work work = new Work();
                    for(RiverWork rw :rr.getWorks()){
                        work.setIsbn(rw.getIsbn());
                        work.setAuthor(rw.getAuthor());
                        work.setCity(rw.getCity());
                        work.setPublisher(rw.getPublisher());
                        work.setTitle(rw.getTitle());
                        work.setUrl(rw.getUrl());
                        work.setYear(rw.getYear());
                        work.setMediumImage(rw.getMediumImage());
                        work.setSmallImage(rw.getSmallImage());
                        work.setLargeImage(rw.getLargeImage());
                        getModel().addWork(work);
                    }
                    WorksFragment worksFragment = (WorksFragment)getSupportFragmentManager().findFragmentByTag("works");
                    worksFragment.refreshWorks();
                    showQuickToast(getString(R.string.addBookLookupSuccessful));
                }else if(rr.getStatus().equals(RiverResults.ITEM_NOT_FOUND)){
                    showQuickToast(getString(R.string.addBookISBNError));
                }else if(rr.getStatus().equals(RiverResults.PARSE_EXCEPTION)){
                    showQuickToast(getString(R.string.addBookParsingError));
                }else if(rr.getStatus().equals(RiverResults.COMMUNICATION_EXCEPTION)){
                    showQuickToast(getString(R.string.addBookCommunicationError));
                }else if(rr.getStatus().equals(RiverResults.ERROR)){
                    showQuickToast(getString(R.string.addBookGeneralError));
                }else if(rr.getStatus().equals(RiverResults.QUOTA_EXCEEDED)){
                    showQuickToast(getString(R.string.addBookQuotaError));
                }
            }else{
                showQuickToast(getString(R.string.addBookNoLookupError));
            }
        }
    }
    /**
     * Show a toast quickly and at the top of the app.
     * Might replace with nice sherlock box.
     * @param message
     */
    private void showQuickToast(String message){
        ((EverciteApplication)this.getApplication()).showQuickToast(message);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.Theme_Sherlock_Light);
        super.onCreate(savedInstanceState);

        final ActionBar bar = getSupportActionBar();

        setContentView(R.layout.evercite_abs_tab_navigation);
        bar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        //bar.setDisplayOptions(0, ActionBar.DISPLAY_SHOW_TITLE);

        bar.addTab(bar.newTab()
                .setText("Books")
                .setTabListener(new TabListener<WorksFragment>(
                        this, "works", WorksFragment.class)));
        bar.addTab(bar.newTab()
                .setText("Evernote")
                .setTabListener(new TabListener<EvernoteFragment>(
                        this, "evernote", EvernoteFragment.class)));

        if (savedInstanceState != null) {
            bar.setSelectedNavigationItem(savedInstanceState.getInt("tabState", 0));
        }
    }

    public void onCreateSave(Bundle savedInstanceState) {
        setTheme(R.style.Theme_Sherlock_Light);
        super.onCreate(savedInstanceState);

        setContentView(R.layout.evercite_abs_tab_navigation);

        // 1. Create Bar
        getSupportActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

        // 2. Create tabs
        ActionBar.Tab worksTab = getSupportActionBar().newTab().setText("Books");
        ActionBar.Tab evernoteTab = getSupportActionBar().newTab().setText("Evernote");

        // 3. Create fragments
        SherlockListFragment worksFragment = new WorksFragment();
        //SherlockFragment worksFragment = new DummyFragment();
        SherlockFragment evernoteFragment = new EvernoteFragment();

        // 4. Set tab listener
        worksTab.setTabListener(new EverciteTabListener(worksFragment));
        evernoteTab.setTabListener(new EverciteTabListener(evernoteFragment));

        // 5. Add tabs to bar
        getSupportActionBar().addTab(worksTab);
        getSupportActionBar().addTab(evernoteTab);

    }

    EverciteModel getModel(){
        return ((EverciteApplication)getApplication()).getModel();
    }

    protected class EverciteTabListener implements ActionBar.TabListener{

        Fragment fragment = null;
        boolean added = false;

        public EverciteTabListener(Fragment fragment){
            this.fragment = fragment;
        }

        @Override
        public void onTabSelected(ActionBar.Tab tab, FragmentTransaction ft) {
            if(!added){
                ft.add(R.id.fragOut,fragment);
                added = true;
            }else{
                ft.attach(fragment);
            }
        }

        @Override
        public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction ft) {
            //ft.remove(fragment);
            ft.detach(fragment);
        }

        @Override
        public void onTabReselected(ActionBar.Tab tab, FragmentTransaction ft) {
            //To change body of implemented methods use File | Settings | File Templates.
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("tabState", getSupportActionBar().getSelectedTab().getPosition());//getSelectedNavigationIndex());
    }

    public static class TabListener<T extends Fragment> implements ActionBar.TabListener {
        private final SherlockFragmentActivity mActivity;
        private final String mTag;
        private final Class<T> mClass;
        private final Bundle mArgs;
        private Fragment mFragment;

        public TabListener(SherlockFragmentActivity activity, String tag, Class<T> clz) {
            this(activity, tag, clz, null);
        }

        public TabListener(SherlockFragmentActivity activity, String tag, Class<T> clz, Bundle args) {
            mActivity = activity;
            mTag = tag;
            mClass = clz;
            mArgs = args;

            // Check to see if we already have a fragment for this tab, probably
            // from a previously saved state.  If so, deactivate it, because our
            // initial state is that a tab isn't shown.
            mFragment = mActivity.getSupportFragmentManager().findFragmentByTag(mTag);
            if (mFragment != null && !mFragment.isDetached()) {
                FragmentTransaction ft = mActivity.getSupportFragmentManager().beginTransaction();
                ft.detach(mFragment);
                ft.commit();
            }
        }

        public void onTabSelected(ActionBar.Tab tab, FragmentTransaction ft) {
            if (mFragment == null) {
                mFragment = Fragment.instantiate(mActivity, mClass.getName(), mArgs);
                ft.add(android.R.id.content, mFragment, mTag);
            } else {
                ft.attach(mFragment);
            }
        }

        public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction ft) {
            if (mFragment != null) {
                ft.detach(mFragment);
            }
        }

        public void onTabReselected(ActionBar.Tab tab, FragmentTransaction ft) {
            Toast.makeText(mActivity, "Reselected!", Toast.LENGTH_SHORT).show();
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (requestCode == 0) {
            if (resultCode == RESULT_OK) {
                String contents = intent.getStringExtra("SCAN_RESULT");
                String format = intent.getStringExtra("SCAN_RESULT_FORMAT");
                new GetBook(contents).execute(true);
            } else if (resultCode == RESULT_CANCELED) {
                showQuickToast(getString(R.string.addBookScanError));
            }
        }
    }

}
