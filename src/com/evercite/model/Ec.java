package com.evercite.model;

/**
 * Object containing Evernote credentials.
 *
 */
public class Ec {
	private boolean valid = false;
	private Integer id=-1;
	private String userId="";
	private String authToken="";
	private String webApiUrlPrefix="";
	private String noteStoreUrl="";
	private String dateAdded="";
	private String lastUsed="";
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getAuthToken() {
		return authToken;
	}
	public void setAuthToken(String authToken) {
		this.authToken = authToken;
	}
	public String getWebApiUrlPrefix() {
		return webApiUrlPrefix;
	}
	public void setWebApiUrlPrefix(String webApiUrlPrefix) {
		this.webApiUrlPrefix = webApiUrlPrefix;
	}
	public String getNoteStoreUrl() {
		return noteStoreUrl;
	}
	public void setNoteStoreUrl(String noteStoreUrl) {
		this.noteStoreUrl = noteStoreUrl;
	}
	public String getDateAdded() {
		return dateAdded;
	}
	public void setDateAdded(String dateAdded) {
		this.dateAdded = dateAdded;
	}
	public String getLastUsed() {
		return lastUsed;
	}
	public void setLastUsed(String lastUsed) {
		this.lastUsed = lastUsed;
	}
	public boolean isValid() {
		return valid;
	}
	public void setValid(boolean valid) {
		this.valid = valid;
	}
}
