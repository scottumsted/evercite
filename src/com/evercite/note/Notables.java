package com.evercite.note;

import java.util.List;

import com.evercite.model.Work;
import android.text.TextUtils;
/**
 *
 * MLA
 * Author's last name, first name. Book titlein italics. Additional information. City of publication: Publishing company, publication date.
 * Allen, Thomas B. Vanishing Wildlife of North America. Washington, D.C.: National Geographic Society, 1974.
 *
 * APA with hanging indents
 * author (date). title in italics: publisher.
 * Allen, T. (1974). Vanishing wildlife of North America. Washington, D.C.: National Geographic Society.
 * 
 * @author scottumsted
 *
 */
public class Notables {
	
	public static final String MLA_TYPE="MLA";
	public static final String APA_TYPE="APA";
	public static final String DETAILS_TYPE="Details";
	
	private static final String MLA_FORMAT = "<p>%s. <i>%s.</i> %s:%s, %s.</p>\n";
	private static final String APA_FORMAT = "<p>%s (%s). <i>%s.</i> %s:%s.</p>\n";
	private static final String DETAILS_FORMAT = "<p>%s (%s). <a href=\"%s\"><i>%s.</i></a> %s:%s.<br/>Notes: %s</p>";
	
	private static final String ENML_FORMAT = 
	    "<?xml version=\"1.0\" encoding=\"UTF-8\"?><!DOCTYPE en-export SYSTEM \"http://xml.evernote.com/pub/evernote-export.dtd\">" +
	    "<en-export><note><title>%s</title><content><![CDATA[\n<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
	    "<!DOCTYPE en-note SYSTEM \"http://xml.evernote.com/pub/enml2.dtd\">" +
	    "<en-note>\n%s\n</en-note>\n]]></content></note></en-export>\n";

	private static final String ENEX_FORMAT = 
		    "<?xml version=\"1.0\" encoding=\"UTF-8\"?><!DOCTYPE en-note SYSTEM \"http://xml.evernote.com/pub/enml2.dtd\">" +
		    "<en-note>\n%s\n</en-note>\n\n";

	public Notables(){
		
	}
	
	public static String createNoteEnml(String title, List<Work> works,String type){
		StringBuffer note = new StringBuffer();
		if(type.equals(MLA_TYPE)){
			for(Work work:works){
				note.append(String.format(MLA_FORMAT, TextUtils.htmlEncode(work.getAuthor()),TextUtils.htmlEncode(work.getTitle()),TextUtils.htmlEncode(work.getCity()),TextUtils.htmlEncode(work.getPublisher()),TextUtils.htmlEncode(work.getYear())));
			}
		}else if(type.equals(APA_TYPE)){
			for(Work work:works){
				note.append(String.format(APA_FORMAT, TextUtils.htmlEncode(work.getAuthor()),TextUtils.htmlEncode(work.getYear()),TextUtils.htmlEncode(work.getTitle()),TextUtils.htmlEncode(work.getCity()),TextUtils.htmlEncode(work.getPublisher())));
			}
		} else if(type.equals(DETAILS_TYPE)){
			for(Work work:works){
				note.append(String.format(DETAILS_FORMAT, TextUtils.htmlEncode(work.getAuthor()),TextUtils.htmlEncode(work.getYear()),TextUtils.htmlEncode(work.getUrl()),TextUtils.htmlEncode(work.getTitle()),TextUtils.htmlEncode(work.getCity()),TextUtils.htmlEncode(work.getPublisher()),TextUtils.htmlEncode(work.getNote())));
			}
		}
		return String.format(ENML_FORMAT, TextUtils.htmlEncode(title), note.toString());
	}

	public static String createNoteEnex(List<Work> works,String type){
		StringBuffer note = new StringBuffer();
		if(type.equals(MLA_TYPE)){
			for(Work work:works){
				note.append(String.format(MLA_FORMAT, TextUtils.htmlEncode(work.getAuthor()),TextUtils.htmlEncode(work.getTitle()),TextUtils.htmlEncode(work.getCity()),TextUtils.htmlEncode(work.getPublisher()),TextUtils.htmlEncode(work.getYear())));
			}
		}else if(type.equals(APA_TYPE)){
			for(Work work:works){
				note.append(String.format(APA_FORMAT, TextUtils.htmlEncode(work.getAuthor()),TextUtils.htmlEncode(work.getYear()),TextUtils.htmlEncode(work.getTitle()),TextUtils.htmlEncode(work.getCity()),TextUtils.htmlEncode(work.getPublisher())));
			}
		} else if(type.equals(DETAILS_TYPE)){
			for(Work work:works){
				note.append(String.format(DETAILS_FORMAT, TextUtils.htmlEncode(work.getAuthor()),TextUtils.htmlEncode(work.getYear()),TextUtils.htmlEncode(work.getUrl()),TextUtils.htmlEncode(work.getTitle()),TextUtils.htmlEncode(work.getCity()),TextUtils.htmlEncode(work.getPublisher()),TextUtils.htmlEncode(work.getNote())));
			}
		}
		return String.format(ENEX_FORMAT, note.toString());
	}
}
