package com.evercite.abs;


import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;
import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.view.SubMenu;
import com.evercite.EverciteApplication;
import com.evercite.R;
import com.evercite.model.EverciteModel;
import com.evercite.model.Work;
import com.river.RiverLookup;
import com.river.RiverResults;
import com.river.RiverWork;

/**
 * Created with IntelliJ IDEA.
 * User: scottumsted
 * Date: 7/5/12
 * Time: 11:59 PM
 * To change this template use File | Settings | File Templates.
 */
public class EverciteABSActivity extends SherlockFragmentActivity {

    private static final int MAX_LOOKUP_TRIES = 3;



    @Override
    public void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.Theme_Sherlock_Light);
        super.onCreate(savedInstanceState);

        final ActionBar bar = getSupportActionBar();



        setContentView(R.layout.evercite_abs_frag_out);
        WorksFragment worksFragment = new WorksFragment();
        FragmentTransaction fragmentTransaction = this.getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(android.R.id.content,worksFragment);
        fragmentTransaction.commit();
//
//        setContentView(R.layout.evercite_abs_tab_navigation);
//        bar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
//        //bar.setDisplayOptions(0, ActionBar.DISPLAY_SHOW_TITLE);
//
//        bar.addTab(bar.newTab()
//                .setText("Books")
//                .setTabListener(new TabListener<WorksFragment>(
//                        this, "works", WorksFragment.class)));
//        bar.addTab(bar.newTab()
//                .setText("Evernote")
//                .setTabListener(new TabListener<EvernoteFragment>(
//                        this, "evernote", EvernoteFragment.class)));
//
//        if (savedInstanceState != null) {
//            bar.setSelectedNavigationItem(savedInstanceState.getInt("tabState", 0));
//        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //Used to put dark icons on light action bar
        //boolean isLight = SampleList.THEME == R.style.Theme_Sherlock_Light;

        // Search
        SubMenu searchSubMenu = menu.addSubMenu("Search");
        MenuItem scanItem = searchSubMenu.add("Scan");
        MenuItem typeItem = searchSubMenu.add("Type");

        MenuItem searchMenuItem = searchSubMenu.getItem();
        searchMenuItem.setIcon(R.drawable.ic_search_gray);
        searchMenuItem.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);

        // Share
        SubMenu shareSubMenu = menu.addSubMenu("Share");
        MenuItem evernoteItem = shareSubMenu.add("Evernote");
        MenuItem otherShareItem = shareSubMenu.add("Other");

        MenuItem shareMenuItem = shareSubMenu.getItem();
        shareMenuItem.setIcon(R.drawable.ic_share_gray);
        shareMenuItem.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);

        // Follow
        MenuItem followMenuItem = menu.add("Follow");
        followMenuItem.setIcon(R.drawable.ic_all_gray);
        followMenuItem.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
        followMenuItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                showInformationDialog("Follow");
                return true;
            }
        });

        // Books
        SubMenu booksSubMenu = menu.addSubMenu("Share");
        MenuItem myBooksItem = booksSubMenu.add("My Books");
        MenuItem allBooksItem = booksSubMenu.add("All Books");

        MenuItem booksMenuItem = booksSubMenu.getItem();
        booksMenuItem.setIcon(R.drawable.ic_book_gray);
        booksMenuItem.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);

//        MenuItem searchMenuItem = menu.add("Search");
//        searchMenuItem.setIcon(R.drawable.ic_search_gray);
//        searchMenuItem.setActionView(R.layout.works_main);
//        searchMenuItem.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM | MenuItem.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW );
//        final View searchView = searchMenuItem.getActionView();
//
//        // Add Button Click
//        final ImageButton addButton = (ImageButton) searchView.findViewById(R.id.add);
//        addButton.setOnClickListener(new View.OnClickListener() {
//            public void onClick(View v) {
//                final EditText isbnEntry = (EditText) searchView.findViewById(R.id.isbnEntry);
//                new GetBook(isbnEntry.getText().toString()).execute(true);
//                //isbnEntry.setText("");
//                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
//                imm.hideSoftInputFromWindow(isbnEntry.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
//            }
//        });
//
//        // Scan Button Clicked
//        final ImageButton scanButton = (ImageButton) searchView.findViewById(R.id.scan);
//        scanButton.setOnClickListener(new View.OnClickListener() {
//            public void onClick(View v) {
//                final EditText isbnEntry = (EditText) searchView.findViewById(R.id.isbnEntry);
//                isbnEntry.setText("");
//                Intent intent = new Intent("com.google.zxing.client.android.SCAN");
//                intent.putExtra("SCAN_MODE", "PRODUCT_MODE");
//                startActivityForResult(intent, 0);
//            }
//        });
//
//        MenuItem evernoteMenuItem = menu.add("Share");
//        evernoteMenuItem.setIcon(R.drawable.ic_share_gray);
//        evernoteMenuItem.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
//        evernoteMenuItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
//            public boolean onMenuItemClick(MenuItem item) {
//                // TODO: display evernote dialog?
//                return true;
//            }
//        });

//        MenuItem followMenuItem = menu.add("Follow");
//        followMenuItem.setIcon(R.drawable.ic_all_gray);
//        followMenuItem.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
//        followMenuItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
//            public boolean onMenuItemClick(MenuItem item) {
//                // TODO: display evernote dialog?
//                return true;
//            }
//        });
//
//        MenuItem booksMenuItem = menu.add("Books");
//        booksMenuItem.setIcon(R.drawable.ic_book_gray);
//        booksMenuItem.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
//        booksMenuItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
//            public boolean onMenuItemClick(MenuItem item) {
//                // TODO: display evernote dialog?
//                return true;
//            }
//        });
//

//        // add scan button and click listener
//        MenuItem scanMenuItem = menu.add("Scan");
//        scanMenuItem.setIcon(R.drawable.ic_scan_gray);
//        scanMenuItem.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
//        scanMenuItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
//            public boolean onMenuItemClick(MenuItem item) {
//                Intent intent = new Intent("com.google.zxing.client.android.SCAN");
//                intent.putExtra("SCAN_MODE", "PRODUCT_MODE");
//                startActivityForResult(intent, 0);
//                return true;
//            }
//        });

        return true;
    }

    /**
     * GetBook is an AsyncTask that calls out to aws to retrieve book
     * details.
     */
    private class GetBook extends AsyncTask<Boolean, Void, RiverResults> {
        String isbn;
        public GetBook(String isbn){
            this.isbn = isbn;
        }

        //protected RiverResults doInBackground(String... isbn) {
        protected RiverResults doInBackground(Boolean... doIt) {
            if(doIt!=null && doIt[0]){
                if(isbn!=null && isbn.length()>0){
                    RiverLookup rl = new RiverLookup();
                    RiverResults rr  = null;
                    for(int i=0;i<MAX_LOOKUP_TRIES;i++){
                        rr = rl.lookupISBN(isbn);
                        if(rr.getStatus().equals(RiverResults.ITEM_FOUND)||rr.getStatus().equals(RiverResults.ITEM_NOT_FOUND)){
                            break;
                        }
                    }
                    return rr;
                }
            }
            return null;
        }

        protected void onPostExecute(RiverResults rr) {
            if(rr != null){
                if(rr.getStatus().equals(RiverResults.ITEM_FOUND)||rr.getStatus().equals(RiverResults.MULTIPLE_ITEMS_FOUND)){
                    Work work = new Work();
                    for(RiverWork rw :rr.getWorks()){
                        work.setIsbn(rw.getIsbn());
                        work.setAuthor(rw.getAuthor());
                        work.setCity(rw.getCity());
                        work.setPublisher(rw.getPublisher());
                        work.setTitle(rw.getTitle());
                        work.setUrl(rw.getUrl());
                        work.setYear(rw.getYear());
                        work.setMediumImage(rw.getMediumImage());
                        work.setSmallImage(rw.getSmallImage());
                        work.setLargeImage(rw.getLargeImage());
                        getModel().addWork(work);
                    }
                    WorksFragment worksFragment = (WorksFragment)getSupportFragmentManager().findFragmentByTag("works");
                    worksFragment.refreshWorks();
                    showQuickToast(getString(R.string.addBookLookupSuccessful));
                }else if(rr.getStatus().equals(RiverResults.ITEM_NOT_FOUND)){
                    showQuickToast(getString(R.string.addBookISBNError));
                }else if(rr.getStatus().equals(RiverResults.PARSE_EXCEPTION)){
                    showQuickToast(getString(R.string.addBookParsingError));
                }else if(rr.getStatus().equals(RiverResults.COMMUNICATION_EXCEPTION)){
                    showQuickToast(getString(R.string.addBookCommunicationError));
                }else if(rr.getStatus().equals(RiverResults.ERROR)){
                    showQuickToast(getString(R.string.addBookGeneralError));
                }else if(rr.getStatus().equals(RiverResults.QUOTA_EXCEEDED)){
                    showQuickToast(getString(R.string.addBookQuotaError));
                }
            }else{
                showQuickToast(getString(R.string.addBookNoLookupError));
            }
        }
    }
    /**
     * Show a toast quickly and at the top of the app.
     * Might replace with nice sherlock box.
     * @param message
     */
    private void showQuickToast(String message){
        ((EverciteApplication)this.getApplication()).showQuickToast(message);
    }


    EverciteModel getModel(){
        return ((EverciteApplication)getApplication()).getModel();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        //outState.putInt("tabState", getSupportActionBar().getSelectedTab().getPosition());//getSelectedNavigationIndex());
    }

    public static class TabListener<T extends Fragment> implements ActionBar.TabListener {
        private final SherlockFragmentActivity mActivity;
        private final String mTag;
        private final Class<T> mClass;
        private final Bundle mArgs;
        private Fragment mFragment;

        public TabListener(SherlockFragmentActivity activity, String tag, Class<T> clz) {
            this(activity, tag, clz, null);
        }

        public TabListener(SherlockFragmentActivity activity, String tag, Class<T> clz, Bundle args) {
            mActivity = activity;
            mTag = tag;
            mClass = clz;
            mArgs = args;

            // Check to see if we already have a fragment for this tab, probably
            // from a previously saved state.  If so, deactivate it, because our
            // initial state is that a tab isn't shown.
            mFragment = mActivity.getSupportFragmentManager().findFragmentByTag(mTag);
            if (mFragment != null && !mFragment.isDetached()) {
                FragmentTransaction ft = mActivity.getSupportFragmentManager().beginTransaction();
                ft.detach(mFragment);
                ft.commit();
            }
        }

        public void onTabSelected(ActionBar.Tab tab, FragmentTransaction ft) {
            if (mFragment == null) {
                mFragment = Fragment.instantiate(mActivity, mClass.getName(), mArgs);
                ft.add(android.R.id.content, mFragment, mTag);
            } else {
                ft.attach(mFragment);
            }
        }

        public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction ft) {
            if (mFragment != null) {
                ft.detach(mFragment);
            }
        }

        public void onTabReselected(ActionBar.Tab tab, FragmentTransaction ft) {
            Toast.makeText(mActivity, "Reselected!", Toast.LENGTH_SHORT).show();
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (requestCode == 0) {
            if (resultCode == RESULT_OK) {
                String contents = intent.getStringExtra("SCAN_RESULT");
                String format = intent.getStringExtra("SCAN_RESULT_FORMAT");
                new GetBook(contents).execute(true);
            } else if (resultCode == RESULT_CANCELED) {
                showQuickToast(getString(R.string.addBookScanError));
            }
        }
    }


    private void showInformationDialog(String message) {
        FragmentManager fm = getSupportFragmentManager();
        InformationDialogFragment informationDialog = InformationDialogFragment.newInstance(message);
        informationDialog.show(fm, "information_dialog");
    }
}
