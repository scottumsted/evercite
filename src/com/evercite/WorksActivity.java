package com.evercite;

import java.util.List;

import com.evercite.model.EverciteModel;
import com.evercite.model.Work;
import com.river.RiverLookup;
import com.river.RiverResults;
import com.river.RiverWork;

import android.app.Dialog;
import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Rect;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

/**
 * WorksActivity shows all books in a listview.
 */
public class WorksActivity extends ListActivity {

	private static final int WORK_VIEW_DIALOG_ID = 0;
	private static final int WORK_CONFIRM_DIALOG_ID = 1;
	private static final int WORK_RIVER_DIALOG_ID = 2;
	private static final int MAX_LOOKUP_TRIES = 3;
	private int selectedWorkId = -1;

    /**
     * Populate layout, set the adapter and define the listeners for the add and scan button.
     * The scan button intents zebra crossing to scan the EAN barcode from the book.
     * TODO: add goggles or iqengines for image scanning
     *
     * @param savedInstanceState
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        List<Work> works= getModel().getWorks();
        ListView listView = getListView();
        listView.setCacheColorHint(R.color.light);
        listView.setDivider( null ); 
        listView.setDividerHeight(0); 

		LayoutInflater inflater = getLayoutInflater();
		ViewGroup header = (ViewGroup)inflater.inflate(R.layout.works_main, listView, false);
		listView.addHeaderView(header,null,false);        
        
		final WorkArrayAdapter workArrayAdapter = new WorkArrayAdapter(this,R.layout.work_item_row,works);
        this.setListAdapter(workArrayAdapter);

        listView.setTextFilterEnabled(true);
        listView.setBackgroundResource(R.color.light);
        listView.setCacheColorHint(Color.parseColor("#00000000"));

        // Click on position within list view, override by adapter click
        listView.setOnItemClickListener(
        		new OnItemClickListener(){
        			 public void onItemClick(AdapterView<?> parent, View view, int position, long id) {              
        				 showQuickToast("item "+position+"clicked");
        		        }
        		}
		);

        // Add Button Click
        final ImageButton addButton = (ImageButton) findViewById(R.id.add);
        addButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                final EditText isbnEntry = (EditText) findViewById(R.id.isbnEntry);
                String isbn = isbnEntry.getEditableText().toString();
                if(isbn!=null && isbn.length()>0){
                	new GetBook().execute(isbn);
                }else{
                	showQuickToast(getString(R.string.addBookISBNError));
                }
            }
        });

        // Scan Button Clicked
        final ImageButton scanButton = (ImageButton) findViewById(R.id.scan);
        scanButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                final EditText isbnEntry = (EditText) findViewById(R.id.isbnEntry);
            	isbnEntry.setText("");
                Intent intent = new Intent("com.google.zxing.client.android.SCAN");
                intent.putExtra("SCAN_MODE", "PRODUCT_MODE");
                startActivityForResult(intent, 0);
           }
        });
        
        
    }


    /**
     * Accept data from other apps through intent filter.
     * TODO: use this code in new activity that we create to specifically handle transfers from other apps
     */
	@Override
	public void onResume(){
		super.onResume();
		// get login status
		// if good store details in ec
		// populate activity with result
		
		
		//old
//    	String isbn = ((EverciteApplication)this.getApplication()).getIntentIsbn();
//    	if(isbn!=null&isbn.length()>0){
//            new GetBook().execute(isbn);
//    	}
//    	((EverciteApplication)this.getApplication()).setIntentIsbn("");
	}

    /**
     * AsynTask to look up a book by isbn and refresh the works list.
     * TODO: add progress
     */
	private class GetBook extends AsyncTask<String, Void, RiverResults> {

    	protected RiverResults doInBackground(String... isbn) {
        	if(isbn!=null && isbn.length>0){
            	RiverLookup rl = new RiverLookup();
        		RiverResults rr  = null;
        		for(int i=0;i<MAX_LOOKUP_TRIES;i++){
                    rr = rl.lookupISBN(isbn[0]);
                	if(rr.getStatus().equals(RiverResults.ITEM_FOUND)||rr.getStatus().equals(RiverResults.ITEM_NOT_FOUND)){
                		break;
                	}
                }
            	return rr;
        	}
        	return null;
        }
        
        protected void onPostExecute(RiverResults rr) {
            if(rr.getStatus().equals(RiverResults.ITEM_FOUND)||rr.getStatus().equals(RiverResults.MULTIPLE_ITEMS_FOUND)){
                Work work = new Work();
            	for(RiverWork rw :rr.getWorks()){
                	work.setIsbn(rw.getIsbn());
                	work.setAuthor(rw.getAuthor());
                	work.setCity(rw.getCity());
                	work.setPublisher(rw.getPublisher());
                	work.setTitle(rw.getTitle());
                	work.setUrl(rw.getUrl());
                	work.setYear(rw.getYear());
                	work.setMediumImage(rw.getMediumImage());
                	work.setSmallImage(rw.getSmallImage());
                	work.setLargeImage(rw.getLargeImage());
                	getModel().addWork(work);                	
            	}
                ((EditText) findViewById(R.id.isbnEntry)).setText("");
                refreshWorks();
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(((EditText) findViewById(R.id.isbnEntry)).getWindowToken(), 0);
            }else if(rr.getStatus().equals(RiverResults.ITEM_NOT_FOUND)){
                ((EditText) findViewById(R.id.isbnEntry)).setText("");
            	showQuickToast(getString(R.string.addBookISBNError));
            }else if(rr.getStatus().equals(RiverResults.PARSE_EXCEPTION)){
            	showQuickToast(getString(R.string.addBookParsingError));
            }else if(rr.getStatus().equals(RiverResults.COMMUNICATION_EXCEPTION)){
            	showQuickToast(getString(R.string.addBookCommunicationError));
            }else if(rr.getStatus().equals(RiverResults.ERROR)){
            	showQuickToast(getString(R.string.addBookGeneralError));
            }else if(rr.getStatus().equals(RiverResults.QUOTA_EXCEEDED)){
            	showQuickToast(getString(R.string.addBookQuotaError));
            }                    	

        }
    }


    /**
     * Show a toast quickly and at the top of the app.
     * Might replace with nice sherlock box.
     * @param message
     */
    private void showQuickToast(String message){
        ((EverciteApplication)getApplication()).showQuickToast(message);
    }

    /**
     * Get the model object shared by the application.
     * @return
     */
    protected EverciteModel getModel(){
        return ((EverciteApplication)getApplication()).getModel();
    }


    /**
     * Refresh the works adapter (and the list) using the wors stored in the db.
     */
    protected void refreshWorks(){
        List<Work> works = getModel().getWorks();
    	((WorkArrayAdapter)this.getListAdapter()).clear();
        for(Work w: works){
        	((WorkArrayAdapter)this.getListAdapter()).add(w);
        }
    }

    /**
     * So when we launch the scanner, the result comes back here. We need to trap the
     * successful result and pass it to the ricer (amazon) thrugh the asynch task.
     *
     * @param requestCode
     * @param resultCode
     * @param intent
     */
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (requestCode == 0) {
            if (resultCode == RESULT_OK) {
                String contents = intent.getStringExtra("SCAN_RESULT");
                String format = intent.getStringExtra("SCAN_RESULT_FORMAT");
                new GetBook().execute(contents);
                refreshWorks();
            } else if (resultCode == RESULT_CANCELED) {
                showQuickToast(getString(R.string.addBookScanError));
            }
        }
    }

    /**
     * Create one of three dialogs on the WorksActivity.
     * @param id
     * @return
     */
    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
        case WORK_VIEW_DIALOG_ID:
        	return this.buildViewDialog();
     
        case WORK_CONFIRM_DIALOG_ID:
        	return this.buildConfirmDialog();

        case WORK_RIVER_DIALOG_ID:
        	return this.buildRiverDialog();
        }
        return null;
    }

    /**
     * Called when we display the dialogs, pull detail in from the db and display in dialog.
     * @param id
     * @param dialog
     */
    @Override
    protected void onPrepareDialog(int id, Dialog dialog) {
        super.onPrepareDialog(id, dialog);
        Work work = getModel().getWork(((EverciteApplication)this.getApplication()).getSelectedWorkId());
        byte[] imageBytes=null;
        switch (id) {
        case WORK_VIEW_DIALOG_ID:
        	ImageButton imageButton = (ImageButton)dialog.findViewById(R.id.workImage);
            imageBytes = RiverLookup.decode(work.getMediumImage()); 
            imageButton.setImageBitmap(BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length));
            dialog.findViewById(R.id.workTitle).requestFocus();
            ((ScrollView)dialog.findViewById(R.id.workScroll)).fullScroll(ScrollView.FOCUS_UP);
        	((TextView)dialog.findViewById(R.id.workTitle)).setText(work.getTitle()); 
        	((TextView)dialog.findViewById(R.id.workAuthor)).setText(work.getAuthor()); 
        	((TextView)dialog.findViewById(R.id.workPublisher)).setText(work.getPublisher()); 
        	((TextView)dialog.findViewById(R.id.workYear)).setText(work.getYear()); 
        	((EditText)dialog.findViewById(R.id.workNote)).setText(work.getNote());
            dialog.findViewById(R.id.workNote).clearFocus();
        	((TextView)dialog.findViewById(R.id.workIsbn)).setText(work.getIsbn()); 
            break;
        case WORK_CONFIRM_DIALOG_ID:
        	TextView titleView = (TextView)dialog.findViewById(R.id.removeDescription);
        	titleView.setText(work.getTitle());  
        	ImageView imageView = (ImageView)dialog.findViewById(R.id.removeImage);
            imageBytes = RiverLookup.decode(work.getLargeImage()); 
            imageView.setImageBitmap(BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length));
        	break;
        case WORK_RIVER_DIALOG_ID:
        	TextView riverTitleView = (TextView)dialog.findViewById(R.id.riverDescription);
        	riverTitleView.setText(work.getTitle());  
        	ImageView riverImageView = (ImageView)dialog.findViewById(R.id.riverImage);
            imageBytes = RiverLookup.decode(work.getLargeImage()); 
            riverImageView.setImageBitmap(BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length));
        	break;
        }
    }

    /**
     * Create the remove confirmation dialog box.
     * @return
     */
    public Dialog buildConfirmDialog(){
    	final Dialog dialog = new Dialog(this);
    	dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
    	Rect dimensions = new Rect();
    	Window window = this.getWindow();
    	window.getDecorView().getWindowVisibleDisplayFrame(dimensions);
    	dialog.setContentView(R.layout.confirm_dialog);
    	dialog.getWindow().setLayout((int)(dimensions.width()*0.9f), (int)(dimensions.height()*0.98f));
    	TextView workView = (TextView)dialog.findViewById(R.id.removeLabel);
    	workView.setText(getString(R.string.removeBookQuestion));
    	workView = (TextView)dialog.findViewById(R.id.removeDescription);
    	workView.setText("");
    	Button yesButton = (Button)dialog.findViewById(R.id.removeYes);
    	yesButton.setOnClickListener(
    			new View.OnClickListener(){
	                    public void onClick(View view) {
	                    	WorksActivity worksActivity = (WorksActivity)dialog.getOwnerActivity();
	                    	EverciteModel model = ((EverciteApplication)(getApplication())).getModel();
	                    	Work work = model.getWork(((EverciteApplication)worksActivity.getApplication()).getSelectedWorkId());
	                    	model.removeWork(work);
	                    	worksActivity.refreshWorks();
	                    	dialog.dismiss();
	                    }
                    }
    			);
    	Button noButton = (Button)dialog.findViewById(R.id.removeNo);
    	noButton.setOnClickListener(
    			new View.OnClickListener(){
	                    public void onClick(View view) {
	                    	dialog.dismiss();
	                    }
                    }
    			);
    	return dialog;
    }

    /**
     * Build the amazon dialog, accessed when clicking the image.
     * @return
     */
    public Dialog buildRiverDialog(){
    	final Dialog dialog = new Dialog(this);
    	dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
    	Rect dimensions = new Rect();
    	Window window = this.getWindow();
    	window.getDecorView().getWindowVisibleDisplayFrame(dimensions);
    	dialog.setContentView(R.layout.river_dialog);
    	dialog.getWindow().setLayout((int)(dimensions.width()*0.9f), (int)(dimensions.height()*0.98f));
    	TextView workView = (TextView)dialog.findViewById(R.id.riverLabel);
    	workView.setText(getString(R.string.amazonQuestion));
    	workView = (TextView)dialog.findViewById(R.id.riverDescription);
    	workView.setText("");
    	Button yesButton = (Button)dialog.findViewById(R.id.riverYes);
    	yesButton.setOnClickListener(
    			new View.OnClickListener(){
	                    public void onClick(View view) {
	                    	WorksActivity worksActivity = (WorksActivity)dialog.getOwnerActivity();
	                    	EverciteModel model = ((EverciteApplication)(getApplication())).getModel();
	                    	WorkArrayAdapter workArrayAdapter = (WorkArrayAdapter)worksActivity.getListAdapter();
	                    	Work work = model.getWork(((EverciteApplication)worksActivity.getApplication()).getSelectedWorkId());
	                        Uri uriUrl = Uri.parse(work.getUrl());
	                        Intent launchBrowser = new Intent(Intent.ACTION_VIEW, uriUrl);  
	                        workArrayAdapter.getContext().startActivity(launchBrowser);  
	                    	dialog.dismiss();
	                    }
                    }
    			);
    	Button noButton = (Button)dialog.findViewById(R.id.riverNo);
    	noButton.setOnClickListener(
    			new View.OnClickListener(){
	                    public void onClick(View view) {
	                    	dialog.dismiss();
	                    }
                    }
    			);
    	return dialog;
    }

    /**
     * Build the view dialog. So they can update the details.
     * @return
     */
    public Dialog buildViewDialog(){
    	final Dialog dialog = new Dialog(this);
    	dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
    	Rect dimensions = new Rect();
    	Window window = this.getWindow();
    	window.getDecorView().getWindowVisibleDisplayFrame(dimensions);
    	dialog.setContentView(R.layout.work_view_dialog);
    	dialog.getWindow().setLayout((int)(dimensions.width()*0.9f), (int)(dimensions.height()*0.9f));
    	ImageButton imageButton = (ImageButton)dialog.findViewById(R.id.workImage);
    	imageButton.setOnClickListener(
			new View.OnClickListener() {
	            public void onClick(View view) {
                	WorksActivity worksActivity = (WorksActivity)dialog.getOwnerActivity();
                	EverciteModel model = ((EverciteApplication)(getApplication())).getModel();
                	WorkArrayAdapter workArrayAdapter = (WorkArrayAdapter)worksActivity.getListAdapter();
                	Work work = model.getWork(((EverciteApplication)worksActivity.getApplication()).getSelectedWorkId());
                    Uri uriUrl = Uri.parse(work.getUrl());
                    Intent launchBrowser = new Intent(Intent.ACTION_VIEW, uriUrl);  
                    workArrayAdapter.getContext().startActivity(launchBrowser);  
	            }
			});
    	Button saveButton = (Button)dialog.findViewById(R.id.workSave);
    	saveButton.setOnClickListener(
    			new View.OnClickListener(){
	                    public void onClick(View view) {
	                    	WorksActivity worksActivity = (WorksActivity)dialog.getOwnerActivity();
	                    	EverciteModel model = ((EverciteApplication)(getApplication())).getModel();
	                    	Work work = model.getWork(((EverciteApplication)worksActivity.getApplication()).getSelectedWorkId());
	                    	work.setNote(((EditText)dialog.findViewById(R.id.workNote)).getText().toString());
	                    	model.updateWork(work);
	                    	worksActivity.refreshWorks();
	                    	dialog.dismiss();
	                    }
                    }
    			);
    	Button cancelButton = (Button)dialog.findViewById(R.id.workCancel);
    	cancelButton.setOnClickListener(
    			new View.OnClickListener(){
	                    public void onClick(View view) {
	                    	dialog.dismiss();
	                    }
                    }
    			);
    	return dialog;
    }

    /**
     * Show the view dialog.
     * @param workId
     */
    public void showViewDialog(int workId){
    	((EverciteApplication)this.getApplication()).setSelectedWorkId(workId);
    	showDialog(WORK_VIEW_DIALOG_ID);
    }

    /**
     * show the remove confirmation dialog.
     * @param workId
     */
    public void showConfirmDialog(int workId){
    	((EverciteApplication)this.getApplication()).setSelectedWorkId(workId);
    	showDialog(WORK_CONFIRM_DIALOG_ID);
    }

    /**
     * Show the amazon dialog.
     * @param workId
     */
    public void showRiverDialog(int workId){
    	((EverciteApplication)this.getApplication()).setSelectedWorkId(workId);
    	showDialog(WORK_RIVER_DIALOG_ID);
    }
}
