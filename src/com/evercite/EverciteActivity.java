package com.evercite;

import android.app.TabActivity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TabHost;
import android.widget.TabHost.OnTabChangeListener;
import android.widget.TabWidget;
import android.widget.TextView;

/**
 * EverciteActivity is the launched activity for Evercite.
 * May be replaced with Sherlock Action Bar. We'll call it EverciteABSActivity.
 *
 * Author: Scott
 *
 */
public class EverciteActivity extends TabActivity implements OnTabChangeListener{
	//public static final int TOPICS_TAB = 0;
	public static final int WORKS_TAB = 0;
	public static final int EVERNOTE_TAB = 1;

    /**
     * Create all the tabs and assign activities to them, then set the default tab
     * to Books.
     *
     * @param savedInstanceState
     */
    @Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.evercite_main);
	    TabHost tabHost = getTabHost();
	    TabHost.TabSpec spec;
	    tabHost.setBackgroundColor(R.color.dark);
	    Intent intent;

	    intent = new Intent().setClass(this, WorksActivity.class);
	    spec = tabHost.newTabSpec(getString(R.string.booksTabName)).setIndicator(getString(R.string.booksTabName))
            .setContent(intent);
	    tabHost.addTab(spec);

	    intent = new Intent().setClass(this, EvernoteActivity.class);
	    spec = tabHost.newTabSpec(getString(R.string.EvernoteTabName)).setIndicator(getString(R.string.EvernoteTabName))
            .setContent(intent);
	    tabHost.addTab(spec);

	    tabHost.setOnTabChangedListener(this);
	    tabHost.setCurrentTab(WORKS_TAB);
	    setTabColors();
	}

    /**
     * Used to accept data from other apps. Not currently used. Will be copied
     * to new specific sharing activity. Requires change to manifest to define
     * intent filter.
     *
     */
	@Override
	public void onResume(){
		super.onResume();
        try{
        	Intent callingIntent = getIntent();
        	if(callingIntent.getType().equals(Intent.ACTION_SEND)){
            	Bundle bundle = callingIntent.getExtras();
            	String intentIsbn = (String)bundle.get(Intent.EXTRA_TEXT);
                if(intentIsbn != null && intentIsbn.trim().length()>0){
                	((EverciteApplication)this.getApplication()).setIntentIsbn(intentIsbn);
                }else{
                	((EverciteApplication)this.getApplication()).setIntentIsbn("");
                }
        	}
        }catch(Exception e){
        	((EverciteApplication)this.getApplication()).setIntentIsbn("");
        }
	}

    /**
     * Change the tab colors and font style when selected.
     *
     * @param tabId
     */
	@Override
	public void onTabChanged(String tabId) {
		setTabColors();
	}

    /**
     * Reset colors and font style.
     */
    private void setTabColors() {
    	TabHost tabHost = getTabHost();
        for (int i = 0; i < tabHost.getTabWidget().getChildCount(); i++) {
            TabWidget tw = tabHost.getTabWidget();
            tw.getChildAt(i).setBackgroundResource(R.color.light);
            tw.getChildAt(i).getLayoutParams().height = 60;
            TextView tv = (TextView)(tw.getChildTabViewAt(i).findViewById(android.R.id.title));
            tv.setTextSize(20);
        }
        tabHost.getTabWidget().getChildAt(tabHost.getCurrentTab()).setBackgroundResource(R.color.dark);
    }
    public void switchTab(int tab){
        this.getTabHost().setCurrentTab(tab);
    }
}