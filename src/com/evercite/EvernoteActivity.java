package com.evercite;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.List;

import org.apache.thrift.TException;
import org.apache.thrift.transport.TTransportException;

import com.evercite.model.Ec;
import com.evercite.model.EverciteModel;
import com.evercite.model.Work;
import com.evercite.note.Notables;
import com.evernote.client.conn.ApplicationInfo;
import com.evernote.client.oauth.android.AuthenticationResult;
import com.evernote.client.oauth.android.EvernoteSession;
import com.evernote.edam.error.EDAMErrorCode;
import com.evernote.edam.error.EDAMNotFoundException;
import com.evernote.edam.error.EDAMSystemException;
import com.evernote.edam.error.EDAMUserException;
import com.evernote.edam.type.Note;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RadioGroup;
import android.widget.Toast;

/**
 * Display a choise of note formatting types, generate notes, and send those notes to Evernote.
 *
 * Author: Scott Umsted
 *
 */
public class EvernoteActivity extends Activity {

	private static final int EVERNOTE_AUTH_DIALOG_ID = 0;
	
	private static final String ECK = "sumsted";
	private static final String ECS = "1295f9bb25b573eb";
	private static final String EH = "sandbox.evernote.com";
	private static final String EAN = "Evercite";
	private static final String EAV = "1.0";
	private static final String APP_DATA_PATH = "/data/com.evercite/files/";
	AuthenticationResult authenticationResult = null;
	EvernoteSession session = null;
	
	public static final int DETAILS_IDX = 0;
	public static final int MLA_IDX = 1;
	public static final int APA_IDX = 2;
	
	public static final String TYPE_TEXT                   = "text/plain";
	public static final String TYPE_ENEX                   = "application/enex";
	
	public List<String> everciteTags = Arrays.asList("Evercite");

	private boolean evernoteAuth = false;

    /**
     * Create the activity and draw the radios and big button.
     *
     * @param savedInstanceState
     */
    @Override
	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
	    setContentView(R.layout.evernote_main);

	    ImageButton evernoteButton = (ImageButton)this.findViewById(R.id.evernote);
        evernoteButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
            	if(session.isLoggedIn()){
            		showQuickToast("evernote click");
    				Log.i("EvernoteActivity","onClick saveNote()");
            		saveNote();
            	}else{
                	evernoteAuth = true;
            		session.authenticate(view.getContext());
            	}
            }
        });
        setupEvernoteSession();
	}


    /**
     * Establish evernote session so we can send notes.
     *
     */
    private void setupEvernoteSession() {
    	evernoteAuth = false;
    	ApplicationInfo applicationInfo = new ApplicationInfo(ECK, ECS, EH, EAN,EAV); 
        EverciteModel model = getModel();
        Ec ec = model.getEc();
        if(ec.isValid()){
        	authenticationResult = new AuthenticationResult(ec.getAuthToken(), ec.getNoteStoreUrl(), ec.getWebApiUrlPrefix(),Integer.parseInt(ec.getUserId()));
        	session = new EvernoteSession(applicationInfo,authenticationResult,getAppDataDirectory()); 
        }else{
        	authenticationResult = null;
        	session = new EvernoteSession(applicationInfo, getAppDataDirectory());
        }
      }

    /**
     * If this was an evernote activity then send a note.
     */
    @Override
    public void onResume() {
    	super.onResume();

    	if(evernoteAuth){
    		evernoteAuth = false;
			// Complete the Evernote authentication process if necessary
			if (!session.completeAuthentication()) {
				this.showQuickToast(getString(R.string.evernoteAuthenticationError));
			}else{
				this.showQuickToast(getString(R.string.evernoteAuthenticationSuccess));
				authenticationResult = session.getAuthenticationResult();
				EverciteModel model = getModel();
				Ec ec = model.getEc();
				ec.setAuthToken(authenticationResult.getAuthToken());
				ec.setNoteStoreUrl(authenticationResult.getNoteStoreUrl());
				ec.setWebApiUrlPrefix(authenticationResult.getWebApiUrlPrefix());
				ec.setUserId(Integer.toString(authenticationResult.getUserId()));
				model.upsertEc(ec);
				Log.i("EvernoteActivity","onResume saveNote()");
				saveNote();
			}
    	}
    }

    /**
     * If we use the intent to write the note we use this guy.
     * Unfortunately inent with enml is not working. We're
     * using the cloud api instead.
     *
     * TODO: remove this in the future
     */
    private class WriteNoteAsync extends AsyncTask<Context, Void, String> {
    	String title;
    	String type;
    	List<Work> works;
    	
    	public WriteNoteAsync(String title, List<Work> works, String type){
    		this.title = title;
    		this.works = works;
    		this.type = type;
    	}

        protected String doInBackground(Context... context) {
        	String note = Notables.createNoteEnml(title, works, type);
        	String result = "Note created";
           
              try {
            	  String noteName = "note.enex";
            	  File path = getAppDataDirectory();
	        	  File file = new File(path,noteName);
	        	  if(file.exists()) {
	        		  file.delete();
	        	  } 
	        	  if(file.createNewFile()){
		              PrintWriter out = new PrintWriter(new FileWriter(file));
		              out.println(note);
		              out.close();
		              Uri uri = Uri.fromFile(file);
		              String us = uri.toString();

		              Intent intent = new Intent();
		              intent.setAction("com.evernote.action.CREATE_NEW_NOTE");
		              intent.putExtra(Intent.EXTRA_TITLE, title);
		              intent.putExtra(Intent.EXTRA_TEXT, note);
		              startActivity(intent);
		                
//		              Intent intent = new Intent();
//		              intent.setAction(Intent.ACTION_SEND);
//		              intent.setDataAndType(uri,TYPE_ENEX);
//		              startActivity(Intent.createChooser(intent, "Share with:"));
		              
	        	  }else{
	        		  result = getString(R.string.evernoteGeneralError);
	        	  }
              } catch (IOException iox) {
            	  result = getString(R.string.evernoteFileError);
              } catch (android.content.ActivityNotFoundException ex) {
            	  result = getString(R.string.evernoteActivityNotFoundError);
              } 
        	return result;
        }
        
        protected void onPostExecute(String status) {
        	showQuickToast(getString(R.string.evernoteTicketNumber));
        }
    }

    /**
     * Create and save note to evernote using the cloud api.
     */
    public void saveNote(){

/*
    	EverciteModel model = getModel();
		List<Work> works = model.getWorks();
    	RadioGroup noteTypeGroup = (RadioGroup)findViewById(R.id.NoteTypeRadio);
		int noteTypeId = noteTypeGroup.getCheckedRadioButtonId();
		String noteType = Notables.DETAILS_TYPE;
		switch(noteTypeId){
		case R.id.mlaType:
			noteType = Notables.MLA_TYPE;
			break;
		case R.id.apaType:
			noteType = Notables.APA_TYPE;
			break;
		case R.id.detailsType:
			noteType = Notables.DETAILS_TYPE;
			break;
		}
		String title = "Evercite "+noteType;
    	String noteContent = Notables.createNoteEnex(works, noteType);
    	
        try {
            Note note = new Note();
            note.setTagNames(everciteTags);
            note.setTitle(title);
            note.setContent(noteContent);
			Note createdNote = session.createNoteStore().createNote(session.getAuthToken(), note);
			this.showQuickToast(getString(R.string.evernoteNoteCreated));
		} catch (TTransportException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (EDAMUserException e) {
			if(e.getErrorCode().equals(EDAMErrorCode.AUTH_EXPIRED)){
				showAuthDialog();
			}
			e.printStackTrace();
		} catch (EDAMSystemException e) {
			e.printStackTrace();
		} catch (EDAMNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
*/
    }


    /**
     * Show a toast at the top of the page.
     * @param message
     */
    private void showQuickToast(String message){
        ((EverciteApplication)getApplication()).showQuickToast(message);
    }

    /**
     * Return the Model object shared by the application.
     *
     * @return
     */
    protected EverciteModel getModel(){
        return ((EverciteApplication)getApplication()).getModel();
    }

    /**
     * Return the application directory for evercite. Create the directory if necessary.
     * @return
     */
    private File getAppDataDirectory(){
    	File path = new File(Environment.getExternalStorageDirectory(),APP_DATA_PATH);
        if(path.exists() == false) {
      	  path.mkdirs();
        }
        return path;
    }


    /**
     * Only one dialog for evercite, the authentication dialog. Create it.
     * @param id
     * @return
     */
    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
        case EVERNOTE_AUTH_DIALOG_ID:
        	return this.buildAuthDialog();
        }
        return null;
    }

    /**
     * When asked to display it populate it with stuff. No stuff here, just a placeholder for now.
     * @param id
     * @param dialog
     */
    @Override
    protected void onPrepareDialog(int id, Dialog dialog) {
        super.onPrepareDialog(id, dialog);
        switch (id) {
        case EVERNOTE_AUTH_DIALOG_ID:
        	break;
        }
    }

    /**
     * Create the authentication dialog.
     * @return
     */
    public Dialog buildAuthDialog(){
    	final Dialog dialog = new Dialog(this);
    	dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
    	Rect dimensions = new Rect();
    	Window window = this.getWindow();
    	window.getDecorView().getWindowVisibleDisplayFrame(dimensions);
    	dialog.setContentView(R.layout.auth_dialog);
    	dialog.getWindow().setLayout((int)(dimensions.width()*0.9f), (int)(dimensions.height()*0.5f));
    	Button yesButton = (Button)dialog.findViewById(R.id.authYes);
    	yesButton.setOnClickListener(
    			new View.OnClickListener(){
	                    public void onClick(View view) {
	                    	session.logOut();
	                    	authenticationResult = null;
	                    	evernoteAuth = true;
	                		session.authenticate(view.getContext());
	                    	dialog.dismiss();
	                    }
                    }
    			);
    	Button noButton = (Button)dialog.findViewById(R.id.authNo);
    	noButton.setOnClickListener(
    			new View.OnClickListener(){
	                    public void onClick(View view) {
	                    	dialog.dismiss();
	                    }
                    }
    			);
    	return dialog;
    }

    /**
     * Show the authentication dialog.
     */
    public void showAuthDialog(){
    	showDialog(EVERNOTE_AUTH_DIALOG_ID);
    }
}
