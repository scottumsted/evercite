package com.evercite.abs;

import android.app.Dialog;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.*;
import com.actionbarsherlock.app.SherlockFragment;
import com.actionbarsherlock.app.SherlockListFragment;
import com.evercite.EverciteApplication;
import com.evercite.R;
import com.evercite.model.Ec;
import com.evercite.model.EverciteModel;
import com.evercite.model.Work;
import com.evercite.note.Notables;
import com.evernote.client.conn.ApplicationInfo;
import com.evernote.client.oauth.android.AuthenticationResult;
import com.evernote.client.oauth.android.EvernoteSession;
import com.evernote.edam.error.EDAMErrorCode;
import com.evernote.edam.error.EDAMNotFoundException;
import com.evernote.edam.error.EDAMSystemException;
import com.evernote.edam.error.EDAMUserException;
import com.evernote.edam.type.Note;
import org.apache.thrift.TException;
import org.apache.thrift.transport.TTransportException;
import org.restlet.engine.util.Base64;

import java.io.File;
import java.util.Arrays;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: scottumsted
 * Date: 7/7/12
 * Time: 12:55 PM
 * To change this template use File | Settings | File Templates.
 */
public class EvernoteFragment extends SherlockFragment {


    private static final int EVERNOTE_AUTH_DIALOG_ID = 0;

    private static final String ECK = "c3Vtc3RlZA==";
    private static final String ECS = "MTI5NWY5YmIyNWI1NzNlYg==";
    private static final String ECK_save = "sumsted";
    private static final String ECS_save = "1295f9bb25b573eb";
    private static final String EH = "sandbox.evernote.com";
    private static final String EAN = "Evercite";
    private static final String EAV = "1.0";
    private static final String APP_DATA_PATH = "/data/com.evercite/files/";
    AuthenticationResult authenticationResult = null;
    EvernoteSession session = null;

    public static final int DETAILS_IDX = 0;
    public static final int MLA_IDX = 1;
    public static final int APA_IDX = 2;

    public static final String TYPE_TEXT                   = "text/plain";
    public static final String TYPE_ENEX                   = "application/enex";

    public List<String> everciteTags = Arrays.asList("Evercite");

    private boolean evernoteAuth = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupEvernoteSession();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState){
        View evernoteView = inflater.inflate(R.layout.evernote_main,container,false);

        // Set spinner options
        Spinner spinner = (Spinner) evernoteView.findViewById(R.id.NoteTypeSpinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(), R.array.NoteTypeArray, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        // Set listener on button
        ImageButton evernoteButton = (ImageButton)evernoteView.findViewById(R.id.evernote);
        evernoteButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if(session.isLoggedIn()){
                    showQuickToast("evernote click");
                    Log.i("EvernoteActivity", "onClick saveNote()");
                    saveNote();
                }else{
                    evernoteAuth = true;
                    session.authenticate(view.getContext());
                }
            }
        });
        return evernoteView;
    }

    @Override
    public void onResume() {
        super.onResume();

        if(evernoteAuth){
            evernoteAuth = false;
            // Complete the Evernote authentication process if necessary
            if (!session.completeAuthentication()) {
                this.showQuickToast(getString(R.string.evernoteAuthenticationError));
            }else{
                this.showQuickToast(getString(R.string.evernoteAuthenticationSuccess));
                authenticationResult = session.getAuthenticationResult();
                EverciteModel model = getModel();
                Ec ec = model.getEc();
                ec.setAuthToken(authenticationResult.getAuthToken());
                ec.setNoteStoreUrl(authenticationResult.getNoteStoreUrl());
                ec.setWebApiUrlPrefix(authenticationResult.getWebApiUrlPrefix());
                ec.setUserId(Integer.toString(authenticationResult.getUserId()));
                model.upsertEc(ec);
                Log.i("EvernoteActivity","onResume saveNote()");
                saveNote();
            }
        }
    }

    /**
     * Create and save note to evernote using the cloud api.
     */
    public void saveNote(){

        EverciteModel model = getModel();
        List<Work> works = model.getWorks();
        Spinner typeSpinner = (Spinner)getActivity().findViewById(R.id.NoteTypeSpinner);
        int typePosition = typeSpinner.getSelectedItemPosition();
        /*
        RadioGroup noteTypeGroup = (RadioGroup)getActivity().findViewById(R.id.NoteTypeRadio);
        int noteTypeId = noteTypeGroup.getCheckedRadioButtonId();
        String noteType = Notables.DETAILS_TYPE;
        switch(noteTypeId){
            case R.id.mlaType:
                noteType = Notables.MLA_TYPE;
                break;
            case R.id.apaType:
                noteType = Notables.APA_TYPE;
                break;
            case R.id.detailsType:
                noteType = Notables.DETAILS_TYPE;
                break;
        }
        */
        String noteType = Notables.DETAILS_TYPE;
        if(typePosition == 0){
            noteType = Notables.DETAILS_TYPE;
        }else if(typePosition == 1){
            noteType = Notables.MLA_TYPE;
        }else if(typePosition == 2){
            noteType = Notables.APA_TYPE;
        }

        String title = "Evercite "+noteType;
        String noteContent = Notables.createNoteEnex(works, noteType);

        try {
            Note note = new Note();
            note.setTagNames(everciteTags);
            note.setTitle(title);
            note.setContent(noteContent);
            Note createdNote = session.createNoteStore().createNote(session.getAuthToken(), note);
            this.showQuickToast(getString(R.string.evernoteNoteCreated));
        } catch (TTransportException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (EDAMUserException e) {
            if(e.getErrorCode().equals(EDAMErrorCode.AUTH_EXPIRED)){
                //showAuthDialog();
            }
            e.printStackTrace();
        } catch (EDAMSystemException e) {
            e.printStackTrace();
        } catch (EDAMNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (TException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }


    /**
     * Show a toast at the top of the page.
     * @param message
     */
    private void showQuickToast(String message){
        ((EverciteApplication)getActivity().getApplication()).showQuickToast(message);
    }

    /**
     * Return the Model object shared by the application.
     *
     * @return
     */
    protected EverciteModel getModel(){
        return ((EverciteApplication)getActivity().getApplication()).getModel();
    }

    /**
     * Return the application directory for evercite. Create the directory if necessary.
     * @return
     */
    private File getAppDataDirectory(){
        File path = new File(Environment.getExternalStorageDirectory(),APP_DATA_PATH);
        if(path.exists() == false) {
            path.mkdirs();
        }
        return path;
    }



    private void setupEvernoteSession() {
        evernoteAuth = false;
        String eck = new String(Base64.decode(ECK));
        String ecs = new String(Base64.decode(ECS));
        ApplicationInfo applicationInfo = new ApplicationInfo(eck, ecs, EH, EAN,EAV);
        EverciteModel model = getModel();
        Ec ec = model.getEc();
        if(ec.isValid()){
            authenticationResult = new AuthenticationResult(ec.getAuthToken(), ec.getNoteStoreUrl(), ec.getWebApiUrlPrefix(),Integer.parseInt(ec.getUserId()));
            session = new EvernoteSession(applicationInfo,authenticationResult,getAppDataDirectory());
        }else{
            authenticationResult = null;
            session = new EvernoteSession(applicationInfo, getAppDataDirectory());
        }
    }


    /**
     * Only one dialog for evercite, the authentication dialog. Create it.
     * @param id
     * @return
    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case EVERNOTE_AUTH_DIALOG_ID:
                //return this.buildAuthDialog();
        }
        return null;
    }
     */


}

