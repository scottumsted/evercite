package com.evercite.abs;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.actionbarsherlock.app.SherlockDialogFragment;
import com.evercite.R;


/**
 * Created with IntelliJ IDEA.
 * User: scottumsted
 * Date: 7/8/12
 * Time: 9:45 PM
 * To change this template use File | Settings | File Templates.
 */
public class WorkDeleteDialogFragment extends SherlockDialogFragment {

    static WorkDeleteDialogFragment newInstance(int workId) {
        WorkDeleteDialogFragment f = new WorkDeleteDialogFragment();

        Bundle args = new Bundle();
        args.putInt("workId", workId);
        f.setArguments(args);

        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        int workId = getArguments().getInt("workId");

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        super.onCreate(savedInstanceState);

        View view = inflater.inflate(R.layout.work_view_dialog, container, false);

        return view;
    }

}
