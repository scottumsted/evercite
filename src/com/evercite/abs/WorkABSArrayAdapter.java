package com.evercite.abs;

import android.app.Activity;
import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.StateListDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.evercite.R;
import com.evercite.WorksActivity;
import com.evercite.model.Work;
import com.river.RiverLookup;

import java.util.List;


/**
 * Array adapter to hold all of our books.
 *
 * Author: Scott Umsted
 *
 */
public class WorkABSArrayAdapter extends ArrayAdapter<Work>{

    Context context;
    int layoutResourceId;
    List<Work> works = null;

    public WorkABSArrayAdapter(Context context, int layoutResourceId, List<Work> works) {
        super(context, layoutResourceId, works);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.works = works;
    }
    
    @Override
    public Work getItem(int position){
    	return (works!=null)?works.get(position):null;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        WorkHolder holder = null;
        
        if(row == null)
        {
            LayoutInflater inflater = ((Activity)context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);
            StateListDrawable stateListDrawable = new StateListDrawable();
            int[] states = new int[] { android.R.attr.state_pressed };
            stateListDrawable.addState(states, new ColorDrawable(0xFFD0D0D0));
            row.setBackgroundDrawable(stateListDrawable);
            row.setOnClickListener(
            		new View.OnClickListener(){
                        public void onClick(View view) {
                            LinearLayout linearLayout = (LinearLayout)view;
                            ListView listView = (ListView)linearLayout.getParent();
                            final int position = listView.getPositionForView(linearLayout)-1;
                            if (position >= 0) {
                            	WorkABSArrayAdapter workArrayAdapter = (WorkABSArrayAdapter)((HeaderViewListAdapter)listView.getAdapter()).getWrappedAdapter();
                                Work work = workArrayAdapter.getItem(position);
                                ((WorksActivity)context).showViewDialog(work.getId());
                            }
                        }
           		});
            holder = new WorkHolder();
            holder.remove = (ImageButton)row.findViewById(R.id.remove);
            holder.river = (ImageButton)row.findViewById(R.id.river);
            holder.txtTitle = (TextView)row.findViewById(R.id.txtTitle);
            holder.txtAuthor = (TextView)row.findViewById(R.id.txtAuthor);
            //holder.txtPublisher = (TextView)row.findViewById(R.id.txtPublisher);
            holder.txtDateAdded = (TextView)row.findViewById(R.id.txtDateAdded);
            
            row.setTag(holder);
        }
        else
        {
            holder = (WorkHolder)row.getTag();
        }
        
        Work work = works.get(position);
        byte[] imageBytes = RiverLookup.decode(work.getMediumImage()); 
        holder.river.setImageBitmap(BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length));

        // If the book cover is clicked on show the amazon dialog
        holder.river.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                LinearLayout linearLayout = (LinearLayout)(view.getParent().getParent());
                ListView listView = (ListView)linearLayout.getParent();
                final int position = listView.getPositionForView(linearLayout)-1;
                if (position >= 0) {
                	WorkABSArrayAdapter workArrayAdapter = (WorkABSArrayAdapter)((HeaderViewListAdapter)listView.getAdapter()).getWrappedAdapter();
                    Work work = workArrayAdapter.getItem(position);
                    ((WorksActivity)context).showRiverDialog(work.getId());
                }
            }
        });
        holder.txtTitle.setText(work.getTitle());
        holder.txtAuthor.setText(work.getAuthor());
        //holder.txtPublisher.setText(work.getPublisher()+", "+work.getYear());
        holder.txtDateAdded.setText(work.getPrettyDateAdded());

        // If the remove icon is clicked show the confirm dialog.
        holder.remove.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                LinearLayout linearLayout = (LinearLayout)(view.getParent().getParent().getParent().getParent());
                ListView listView = (ListView)linearLayout.getParent();
                final int position = listView.getPositionForView(linearLayout)-1;
                if (position >= 0) {
                	WorkABSArrayAdapter workArrayAdapter = (WorkABSArrayAdapter)((HeaderViewListAdapter)listView.getAdapter()).getWrappedAdapter();
                    Work work = workArrayAdapter.getItem(position);
                    ((WorksActivity)context).showConfirmDialog(work.getId());
                }
            }
        });
        return row;
    }
    
    static class WorkHolder
    {
        ImageButton remove;
        ImageButton river;
        TextView txtTitle;
        TextView txtAuthor;
        //TextView txtPublisher;
        TextView txtDateAdded;
    }
}