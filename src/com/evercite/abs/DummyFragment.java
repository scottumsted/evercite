package com.evercite.abs;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.actionbarsherlock.app.SherlockFragment;
import com.evercite.R;

/**
 * Created with IntelliJ IDEA.
 * User: scottumsted
 * Date: 7/15/12
 * Time: 12:48 PM
 * To change this template use File | Settings | File Templates.
 */
public class DummyFragment extends SherlockFragment {


    static DummyFragment newInstance() {
        DummyFragment f = new DummyFragment();

        // Supply num input as an argument.
        //Bundle args = new Bundle();
        //args.putInt("num", num);
        //f.setArguments(args);

        return f;
    }
    @Override
    public View onCreateView(LayoutInflater inflater,ViewGroup container,Bundle savedInstanceState){
        View tabFragView = (View)inflater.inflate(R.layout.dummy,container,false);
        return tabFragView;
    }
}
