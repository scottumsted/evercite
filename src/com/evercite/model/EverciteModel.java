package com.evercite.model;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class EverciteModel extends SQLiteOpenHelper {

	protected static final int dbVersion=17;
	
	protected static final String dbName="everciteDb";
	
	protected static final String topicCreateSql="create table topic (id integer primary key autoincrement, name varchar(100), dateAdded varchar(100))";
	protected static final String topicDropSQL="drop table topic";
	
	protected static final String workCreateSql="create table work (id integer primary key autoincrement, topicId integer, title varchar(200), author varchar(200), "+
								"publisher varchar(100), url varchar(300), city varchar(100), year varchar(100), isbn varchar(100), smallImage text, mediumImage text, largeImage text, note text, dateAdded varchar(100))";
	protected static final String workDropSQL="drop table work";
	
	protected static final String ecCreateSql = "create table ec (id integer primary key autoincrement, authToken text, noteStoreUrl text, webApiUrlPrefix text, userId text, dateAdded varchar(100), lastUsed varchar(100))";
	protected static final String ecDropSQL="drop table ec";
	
	public EverciteModel(Context context) {
		super(context, dbName, null, dbVersion);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		try{
			db.execSQL(topicCreateSql);
			db.execSQL(workCreateSql);
			db.execSQL(ecCreateSql);
		}catch(Exception e){
			Log.v("onCreate:create:", e.getMessage());
		}

		try{
			//this.dml(db);
		}catch(Exception e){
			Log.v("onCreate:dml:", e.getMessage());
		}
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		try{
			//db.execSQL(topicDropSQL);
			//db.execSQL(workDropSQL);
			db.execSQL(ecDropSQL);
		}catch(Exception e){
			Log.v("onUpgrade:drop:", e.getMessage());
		}

		try{
			//db.execSQL(topicCreateSql);
			//db.execSQL(workCreateSql);
			db.execSQL(ecCreateSql);
		}catch(Exception e){
			Log.v("onUpgrade:create:", e.getMessage());
		}
		
		try{
			//this.dml(db);
		}catch(Exception e){
			Log.v("onUpgrade:dml:", e.getMessage());
		}
	}
	
	private void dml(SQLiteDatabase db){
		int count = 0;
		try{
			count = db.delete("topic", null, null);
		}catch(Exception e){
			Log.v("dml", e.getMessage());
		}finally{
			//try{db.close();}catch(Exception e){}
		}
		Topic topic = new Topic();
		ContentValues values = new ContentValues();
   		topic.setName("Popcorn");
		values.put("name", topic.getName());
   		values.put("dateAdded", todaysDate());
   		db.insert("topic", null, values);
		topic.setName("Stringbean");
		values.put("name", topic.getName());
   		values.put("dateAdded", todaysDate());
   		db.insert("topic", null, values);
		topic.setName("Cabbage");
		values.put("name", topic.getName());
   		values.put("dateAdded", todaysDate());
   		db.insert("topic", null, values);
		topic.setName("Stringbean");
		values.put("name", topic.getName());
   		values.put("dateAdded", todaysDate());
   		db.insert("topic", null, values);
		topic.setName("Cabbage");
		values.put("name", topic.getName());
   		values.put("dateAdded", todaysDate());
   		db.insert("topic", null, values);
		topic.setName("Cabbage");
		values.put("name", topic.getName());
   		values.put("dateAdded", todaysDate());
   		db.insert("topic", null, values);
		topic.setName("Stringbean");
		values.put("name", topic.getName());
   		values.put("dateAdded", todaysDate());
   		db.insert("topic", null, values);
		topic.setName("Cabbage");
		values.put("name", topic.getName());
   		values.put("dateAdded", todaysDate());
   		db.insert("topic", null, values);
		topic.setName("Cabbage");
		values.put("name", topic.getName());
   		values.put("dateAdded", todaysDate());
   		db.insert("topic", null, values);
		topic.setName("Cabbage");
		values.put("name", topic.getName());
   		values.put("dateAdded", todaysDate());
   		db.insert("topic", null, values);
		topic.setName("Stringbean");
		values.put("name", topic.getName());
   		values.put("dateAdded", todaysDate());
   		db.insert("topic", null, values);
		topic.setName("Cabbage");
		values.put("name", topic.getName());
   		values.put("dateAdded", todaysDate());
   		db.insert("topic", null, values);
		topic.setName("Cabbage");
		values.put("name", topic.getName());
   		values.put("dateAdded", todaysDate());
   		db.insert("topic", null, values);
		count = 0;
		try{
			count = db.delete("work", null, null);
		}catch(Exception e){
			Log.v("dml", e.getMessage());
		}finally{
			//try{db.close();}catch(Exception e){}
		}
		values.clear();
		values.put("author","Johnson, A.");
		values.put("city","New York");
		values.put("isbn","1231231231233");
		values.put("publisher","Tabor");
		values.put("title","Got Bugs");
		values.put("url","http://theverge.com");
   		values.put("dateAdded", todaysDate());
		db.insert("work", null, values);
		values.put("author","Smith, A.");
		values.put("city","New York");
		values.put("isbn","1231231231233");
		values.put("publisher","Tabor");
		values.put("title","Lamps Are Cool");
		values.put("url","http://theverge.com");
		db.insert("work", null, values);
		values.put("author","Johnson, A.");
		values.put("city","New York");
		values.put("isbn","1231231231233");
		values.put("publisher","Tabor");
		values.put("title","Cats");
		values.put("url","http://theverge.com");
		db.insert("work", null, values);
		values.put("author","Johnson, A.");
		values.put("city","New York");
		values.put("isbn","1231231231233");
		values.put("publisher","Tabor");
		values.put("title","Mice");
		values.put("url","http://theverge.com");
		db.insert("work", null, values);
	}

	public List<Topic> getTopics(){
		List<Topic> topics = new ArrayList<Topic>();
		
		SQLiteDatabase db = null;
		Cursor cursor = null;
		try{
			db = this.getWritableDatabase();
			cursor = db.query("topic", null, null, null, null,null, "id desc");
			cursor.moveToFirst();
	        while (cursor.isAfterLast() == false) {
	        	Topic topic = new Topic();
	        	topic.setName(cursor.getString(cursor.getColumnIndex("name")));
	        	topic.setId(cursor.getInt(cursor.getColumnIndex("id")));
	        	topic.setDateAdded(cursor.getString(cursor.getColumnIndex("dateAdded")));
	        	topics.add(topic);
	        	cursor.moveToNext();
	        }
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			try{cursor.close();db.close();}catch(Exception e){}
		}		   
		   
		return topics;
	}
	
	public Topic getTopic(Integer id){
		Topic topic = new Topic();
		
		SQLiteDatabase db = null;
		Cursor cursor = null;
		try{
			db = this.getWritableDatabase();
			cursor = db.query("topic", null,"id=?", new String[]{Integer.toString(id)}, null, null, null);
			cursor.moveToFirst();
	        while (cursor.isAfterLast() == false) {
	        	topic.setName(cursor.getString(cursor.getColumnIndex("name")));
	        	topic.setId(cursor.getInt(cursor.getColumnIndex("id")));
	        	topic.setDateAdded(cursor.getString(cursor.getColumnIndex("dateAdded")));
	        	break;
	        }
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			try{cursor.close();db.close();}catch(Exception e){}
		}		   
		
		return topic;
	}
	
	private String todaysDate(){
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd"); 
		Date date = new Date();
		return dateFormat.format(date);
	}
	
	public long addTopic(Topic topic){
		long id = -1;
		SQLiteDatabase db = null;
		try{
			db = this.getWritableDatabase();
			ContentValues values = new ContentValues();
	   		values.put("name", topic.getName());
	   		values.put("dateAdded", todaysDate());
	   		id = db.insert("topic", null, values);
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			try{db.close();}catch(Exception e){}
		}
		return id;
	}
	
	public int updateTopic(Topic topic){
		int count = -1;
		SQLiteDatabase db = null;
		try{
			db = this.getWritableDatabase();
			ContentValues values = new ContentValues();
			values.put("name", topic.getName());
			db.update("topic", values, "id=?", new String []{String.valueOf(topic.getId())});   
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			try{db.close();}catch(Exception e){}
			count = 0;
		}
		return count;
	}

	public int removeTopic(Topic topic){
		int count = -1;
		SQLiteDatabase db = null;
		try{
			db = this.getWritableDatabase();
			count = db.delete("topic", "id=?", new String []{String.valueOf(topic.getId())});   
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			try{db.close();}catch(Exception e){}
		}
		return count;
	}
	
	public List<Work> getWorks(Topic topic){
		List<Work> works = new ArrayList<Work>();

		SQLiteDatabase db = null;
		Cursor cursor = null;
		try{
			db = this.getWritableDatabase();
			cursor = db.query("work", null, "topicId=?", new String[]{Integer.toString(topic.getId())}, null, null, "id desc");
			cursor.moveToFirst();
	        while (cursor.isAfterLast() == false) {
	        	Work work = new Work();
	        	work.setAuthor(cursor.getString(cursor.getColumnIndex("author")));
	        	work.setCity(cursor.getString(cursor.getColumnIndex("city")));
	        	work.setId(cursor.getInt(cursor.getColumnIndex("id")));
	        	work.setIsbn(cursor.getString(cursor.getColumnIndex("isbn")));
	        	work.setPublisher(cursor.getString(cursor.getColumnIndex("publisher")));
	        	work.setTitle(cursor.getString(cursor.getColumnIndex("title")));
	        	work.setUrl(cursor.getString(cursor.getColumnIndex("url")));
	        	work.setYear(cursor.getString(cursor.getColumnIndex("year")));
	        	work.setSmallImage(cursor.getString(cursor.getColumnIndex("smallImage")));
	        	work.setMediumImage(cursor.getString(cursor.getColumnIndex("mediumImage")));
	        	work.setLargeImage(cursor.getString(cursor.getColumnIndex("largeImage")));
	        	work.setDateAdded(cursor.getString(cursor.getColumnIndex("dateAdded")));
	        	work.setNote(cursor.getString(cursor.getColumnIndex("note")));
	        	works.add(work);
	        	cursor.moveToNext();
	        }
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			try{cursor.close();db.close();}catch(Exception e){}
		}		   

		return works;
	}

	
	public List<Work> getWorks(){
		List<Work> works = new ArrayList<Work>();

		SQLiteDatabase db = null;
		Cursor cursor = null;
		try{
			db = this.getWritableDatabase();
			cursor = db.query("work", null, null, null, null, null, "id desc");
			cursor.moveToFirst();
	        while (cursor.isAfterLast() == false) {
	        	Work work = new Work();
	        	work.setAuthor(cursor.getString(cursor.getColumnIndex("author")));
	        	work.setCity(cursor.getString(cursor.getColumnIndex("city")));
	        	work.setId(cursor.getInt(cursor.getColumnIndex("id")));
	        	work.setIsbn(cursor.getString(cursor.getColumnIndex("isbn")));
	        	work.setPublisher(cursor.getString(cursor.getColumnIndex("publisher")));
	        	work.setTitle(cursor.getString(cursor.getColumnIndex("title")));
	        	work.setUrl(cursor.getString(cursor.getColumnIndex("url")));
	        	work.setYear(cursor.getString(cursor.getColumnIndex("year")));
	        	work.setSmallImage(cursor.getString(cursor.getColumnIndex("smallImage")));
	        	work.setMediumImage(cursor.getString(cursor.getColumnIndex("mediumImage")));
	        	work.setLargeImage(cursor.getString(cursor.getColumnIndex("largeImage")));
	        	work.setDateAdded(cursor.getString(cursor.getColumnIndex("dateAdded")));
	        	work.setNote(cursor.getString(cursor.getColumnIndex("note")));
	        	works.add(work);
	        	cursor.moveToNext();
	        }
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			try{cursor.close();db.close();}catch(Exception e){}
		}		   

		return works;
	}
	public Work getWork(int id){
		Work work = new Work();
		
		SQLiteDatabase db = null;
		Cursor cursor = null;
		try{
			db = this.getWritableDatabase();
			cursor = db.query("work", null,"id=?", new String[]{Integer.toString(id)}, null, null, null);
			cursor.moveToFirst();
	        while (cursor.isAfterLast() == false) {
	        	work.setAuthor(cursor.getString(cursor.getColumnIndex("author")));
	        	work.setCity(cursor.getString(cursor.getColumnIndex("city")));
	        	work.setId(cursor.getInt(cursor.getColumnIndex("id")));
	        	work.setIsbn(cursor.getString(cursor.getColumnIndex("isbn")));
	        	work.setPublisher(cursor.getString(cursor.getColumnIndex("publisher")));
	        	work.setTitle(cursor.getString(cursor.getColumnIndex("title")));
	        	work.setUrl(cursor.getString(cursor.getColumnIndex("url")));
	        	work.setYear(cursor.getString(cursor.getColumnIndex("year")));
	        	work.setSmallImage(cursor.getString(cursor.getColumnIndex("smallImage")));
	        	work.setMediumImage(cursor.getString(cursor.getColumnIndex("mediumImage")));
	        	work.setLargeImage(cursor.getString(cursor.getColumnIndex("largeImage")));
	        	work.setDateAdded(cursor.getString(cursor.getColumnIndex("dateAdded")));
	        	work.setNote(cursor.getString(cursor.getColumnIndex("note")));
	        	break;
	        }
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			try{cursor.close();db.close();}catch(Exception e){}
		}		   
		
		return work;
	}
	
	public long addWork(Work work){
		long id = -1;
		SQLiteDatabase db = null;
		try{
			db = this.getWritableDatabase();
			ContentValues values = new ContentValues();
	   		values.put("author", work.getAuthor());
	   		values.put("city", work.getCity());
	   		values.put("isbn", work.getIsbn());
	   		values.put("publisher", work.getPublisher());
	   		values.put("title", work.getTitle());
	   		values.put("url", work.getUrl());
	   		values.put("year", work.getYear());
			values.put("smallImage", work.getSmallImage());
			values.put("mediumImage", work.getMediumImage());
			values.put("largeImage", work.getLargeImage());
	   		values.put("dateAdded", todaysDate());
	   		values.put("note", work.getNote());
	   		id = db.insert("work", null, values);
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			try{db.close();}catch(Exception e){}
		}
		return id;
	}

	public int updateWork(Work work){
		int count = -1;
		SQLiteDatabase db = null;
		try{
			db = this.getWritableDatabase();
			ContentValues values = new ContentValues();
			values.put("title", work.getTitle());
			values.put("author", work.getAuthor());
			values.put("publisher", work.getPublisher());
			values.put("year", work.getYear());
			values.put("smallImage", work.getSmallImage());
			values.put("mediumImage", work.getMediumImage());
			values.put("largeImage", work.getLargeImage());
			values.put("city", work.getCity());
	   		values.put("note", work.getNote());
			db.update("work", values, "id=?", new String []{String.valueOf(work.getId())});   
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			try{db.close();}catch(Exception e){}
			count = 0;
		}
		return count;
	}

	public int removeWork(Work work){
		int count = -1;
		SQLiteDatabase db = null;
		try{
			db = this.getWritableDatabase();
			count = db.delete("work", "id=?", new String []{String.valueOf(work.getId())});   
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			try{db.close();}catch(Exception e){}
		}
		return count;
	}
	
	public long addEc(Ec ec){
		long id = -1;
		SQLiteDatabase db = null;
		try{
			db = this.getWritableDatabase();
			ContentValues values = new ContentValues();
	   		values.put("lastUsed", ec.getLastUsed());
	   		values.put("authToken", ec.getAuthToken());
	   		values.put("userId", ec.getUserId());
	   		values.put("noteStoreUrl", ec.getNoteStoreUrl());
	   		values.put("webApiUrlPrefix", ec.getWebApiUrlPrefix());
			values.put("dateAdded", todaysDate());
	   		id = db.insert("ec", null, values);
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			try{db.close();}catch(Exception e){}
		}
		return id;
	}
	
	public int upsertEc(Ec ec){
		int count = -1;
		SQLiteDatabase db = null;
		try{
			db = this.getWritableDatabase();
			ContentValues values = new ContentValues();
	   		values.put("authToken", ec.getAuthToken());
	   		values.put("userId", ec.getUserId());
	   		values.put("noteStoreUrl", ec.getNoteStoreUrl());
	   		values.put("webApiUrlPrefix", ec.getWebApiUrlPrefix());
			values.put("lastUsed", todaysDate());
			count = db.update("ec", values, "id=?", new String []{String.valueOf(ec.getId())});   
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			try{db.close();}catch(Exception e){}
		}
		if(count<=0){
			count = (addEc(ec)>0) ? 1 : 0;
		}
		return count;
	}
	
	public Ec getEc(){
		Ec ec = new Ec();

		SQLiteDatabase db = null;
		Cursor cursor = null;
		try{
			db = this.getWritableDatabase();
			cursor = db.query("ec", null, null, null, null, null, null);
			cursor.moveToFirst();
	        while (cursor.isAfterLast() == false) {
	        	ec.setValid(true);
	        	ec.setUserId(cursor.getString(cursor.getColumnIndex("userId")));
	        	ec.setAuthToken(cursor.getString(cursor.getColumnIndex("authToken")));
	        	ec.setNoteStoreUrl(cursor.getString(cursor.getColumnIndex("noteStoreUrl")));
	        	ec.setWebApiUrlPrefix(cursor.getString(cursor.getColumnIndex("webApiUrlPrefix")));
	        	ec.setLastUsed(cursor.getString(cursor.getColumnIndex("lastUsed")));
	        	ec.setId(cursor.getInt(cursor.getColumnIndex("id")));
	        	ec.setDateAdded(cursor.getString(cursor.getColumnIndex("dateAdded")));
	        	break;
	        }
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			try{cursor.close();db.close();}catch(Exception e){}
		}		   
		
		return ec;
	}
}
