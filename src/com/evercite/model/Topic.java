package com.evercite.model;

public class Topic {

	private Integer id;
	private String name;
	private String dateAdded;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDateAdded() {
		return dateAdded;
	}
	public void setDateAdded(String dateAdded) {
		this.dateAdded = dateAdded;
	}
	public String getPrettyDateAdded(){
		String prettyDate = "";
		if(dateAdded!=null && dateAdded.length()==8){
			prettyDate = dateAdded.substring(4, 6)+"/"+dateAdded.substring(6, 8)+"/"+dateAdded.substring(0,4);
		}
		return prettyDate;
	}
}
